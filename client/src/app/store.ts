import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';

import authReducer from '../features/user/authSlice';
import cityReducer from '../features/landing/viewLocationSlice';
import tripReducer from '../features/landing/viewTripsSlice';
import seatReducer from '../features/viewSeats/viewSeatsSlice';
import ticketReducer from '../features/ticket/ticketSlice';
import bookingsReducer from '../features/myBookings/myBookingSplice';
import viewMyTicketReducer from '../features/viewTicket/viewTicketSplice';

export const store = configureStore({
  reducer: {
    auth: authReducer,
    city: cityReducer,
    trips: tripReducer,
    seats: seatReducer,
    ticket: ticketReducer,
    bookings: bookingsReducer,
    viewMyTicket: viewMyTicketReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false }),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
