import React, { Fragment, useEffect } from 'react';
import { Landing } from './features/landing/Landing';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
// import { useAppDispatch } from './app/hooks';
import setAuthToken from './utils/setAuthToken';
import './App.css';
import NavBar from './features/navbar/Navbar';
import RegisterUser from './features/user/Register';
import LoginUser from './features/user/Login';
import ViewSeats from './features/viewSeats/ViewSeats';
// import { authAsync, logOut } from './features/user/authSlice';
import TicketForm from './features/ticket/TicketForm';
import MyBookings from './features/myBookings/MyBookings';
import ViewTicket from './features/viewTicket/ViewTicket';

function App() {
  // const dispatch = useAppDispatch();
  useEffect(() => {
    //   // check for token in LS when app first runs
    if (localStorage.token) {
      // if there is a token set axios headers for all requests
      setAuthToken(localStorage.token);
    }
    //   // try to fetch a user, if no token or invalid token we
    //   // will get a 401 response from our API
    //   dispatch(authAsync(localStorage.token));

    //   // log user out from all tabs if they log out in one tab
    //   window.addEventListener('storage', () => {
    //     if (!localStorage.token) {
    //       dispatch(logOut(''));
    //     }
    //   });

    // eslint-disable-next-line
  }, []);
  return (
    <BrowserRouter>
      <Fragment>
        <NavBar />
        <Routes>
          <Route path='/' element={<Landing />} />
          <Route path='/register' element={<RegisterUser />} />
          <Route path='/login' element={<LoginUser />} />
          <Route path='/viewSeat' element={<ViewSeats />} />
          <Route path='/bookTicket' element={<TicketForm />} />
          <Route path='/myBookings' element={<MyBookings />} />
          <Route path='/viewTicket' element={<ViewTicket />} />
        </Routes>
      </Fragment>
    </BrowserRouter>
  );
}

export default App;
