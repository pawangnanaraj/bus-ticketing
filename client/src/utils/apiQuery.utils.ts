import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const baseQueryApi = createApi({
  reducerPath: 'baseQueryApi',
  baseQuery: fetchBaseQuery({ baseUrl: '/api' }),
  refetchOnMountOrArgChange: 30 * 60,
  endpoints: () => ({}),
});
