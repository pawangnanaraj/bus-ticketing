import {
  createSlice,
  createAsyncThunk,
  PayloadAction,
  Slice,
} from '@reduxjs/toolkit';
import api from '../../utils/api.utils';
import { RootState } from '../../app/store';

export const viewSeatAsync = createAsyncThunk(
  '/viewSeats',
  async (tripId: string, thunkAPI) => {
    try {
      const seats = await api.get(`/ticket/seat/${tripId}`);
      const tripDetails = await api.get(`/trip/${tripId}`);

      return { seats, tripDetails };
    } catch (error: any) {
      const errors = error.response.data;
      return thunkAPI.rejectWithValue(errors);
    }
  }
);

export const availableSeatsAsync = createAsyncThunk(
  '/availableSeats',
  async (tripId: string, thunkAPI) => {
    try {
      const seats = await api.get(`/ticket/available/${tripId}`);
      return seats;
    } catch (error: any) {
      const errors = error.response.data;
      return thunkAPI.rejectWithValue(errors);
    }
  }
);

export const updateSeatStateAsync = createAsyncThunk(
  '/updateSeatState',
  async (seatStatus: { seatId: string[]; status: string }, thunkAPI) => {
    try {
      const seatId = seatStatus.seatId;
      const status = seatStatus.status;
      console.log('status', status);

      const seatDetails = await api.put(`/ticket/seat/${status}`, seatId);
      return seatDetails;
    } catch (error: any) {
      const errors = error.response.data;
      return thunkAPI.rejectWithValue(errors);
    }
  }
);

export const seatDetailsAsync = createAsyncThunk(
  '/seatStatus',
  async (seatId: string[], thunkAPI) => {
    try {
      const seats = await api.post(`/ticket/seat`, seatId);
      return seats;
    } catch (error: any) {
      const errors = error.response.data;
      return thunkAPI.rejectWithValue(errors);
    }
  }
);

export interface SeatState {
  allSeats: [];
  availableSeats: '';
  trip: {
    fromCity: '';
    toCity: '';
    travelStartDate: '';
    travelStartTime: '';
    reachingDate: '';
    reachingTime: '';
    busName: '';
    busType: '';
    pricePerSeat: '';
  };
  selectedSeats: [
    {
      seatId: string;
      seatNumber: number;
    }
  ];
  seatStatus: [];
  status: 'idle' | 'loading' | 'failed';
  isError: Boolean;
  errorMessage: any;
  seatsApproved: Boolean;
}

const initialState: SeatState = {
  allSeats: [],
  availableSeats: '',
  trip: {
    fromCity: '',
    toCity: '',
    travelStartDate: '',
    travelStartTime: '',
    reachingDate: '',
    reachingTime: '',
    busName: '',
    busType: '',
    pricePerSeat: '',
  },
  selectedSeats: [
    {
      seatId: 'a',
      seatNumber: 0,
    },
  ],
  seatStatus: [],
  status: 'idle',
  isError: false,
  errorMessage: [''],
  seatsApproved: false,
};

export const seatSlice: Slice<SeatState> = createSlice({
  name: 'seats',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    selectedSeats: (state, action: PayloadAction<any>) => {
      let a = 0;
      for (let i = 0; i < state.selectedSeats.length; i++) {
        if (state.selectedSeats[i].seatId === action.payload.seatId) {
          state.selectedSeats.splice(i, 1);
          a = 1;
        }
      }
      if (a === 0) {
        state.selectedSeats.push(action.payload);
      }
    },
    clearSeats: (state, action: PayloadAction<any>) => {
      state.allSeats = [];
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(
        seatDetailsAsync.rejected,
        (state, action: PayloadAction<any>) => {
          localStorage.removeItem('selectedSeats');
          state.status = 'failed';
          state.isError = true;
          state.allSeats = [];
          state.availableSeats = '';
          state.errorMessage = action.payload.errors
            ? [action.payload.errors]
            : null;
        }
      )
      .addCase(seatDetailsAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(
        seatDetailsAsync.fulfilled,
        (state, action: PayloadAction<any>) => {
          state.status = 'idle';
          state.seatsApproved = false;
          state.seatStatus = action.payload;
        }
      )
      .addCase(
        updateSeatStateAsync.rejected,
        (state, action: PayloadAction<any>) => {
          localStorage.removeItem('selectedSeats');
          state.status = 'failed';
          state.seatsApproved = false;
          state.isError = true;
          state.allSeats = [];
          state.availableSeats = '';
          state.selectedSeats = [
            {
              seatId: 'a',
              seatNumber: 0,
            },
          ];
          state.errorMessage = action.payload.errors
            ? [action.payload.errors]
            : null;
        }
      )
      .addCase(updateSeatStateAsync.pending, (state) => {
        state.status = 'loading';
        state.seatsApproved = false;
      })
      .addCase(updateSeatStateAsync.fulfilled, (state) => {
        state.status = 'idle';
        state.seatsApproved = true;
      })
      .addCase(availableSeatsAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(
        availableSeatsAsync.fulfilled,
        (state, action: PayloadAction<any>) => {
          state.status = 'idle';
          state.availableSeats = action.payload.data.length;
          state.errorMessage = null;
          state.isError = false;
          state.seatsApproved = false;
        }
      )
      .addCase(
        availableSeatsAsync.rejected,
        (state, action: PayloadAction<any>) => {
          state.status = 'failed';
          state.isError = true;
          state.allSeats = [];
          state.availableSeats = '';

          state.seatsApproved = false;
          state.errorMessage = action.payload.errors
            ? [action.payload.errors]
            : null;
        }
      )
      .addCase(viewSeatAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(viewSeatAsync.fulfilled, (state, action: PayloadAction<any>) => {
        console.log(action.payload);
        state.status = 'idle';
        state.allSeats = action.payload.seats.data;
        state.trip = { ...state.trip, ...action.payload.tripDetails.data };
        state.errorMessage = null;
        state.isError = false;
      })
      .addCase(viewSeatAsync.rejected, (state, action: PayloadAction<any>) => {
        state.status = 'failed';
        state.isError = true;
        state.errorMessage = action.payload.errors
          ? [action.payload.errors]
          : null;
      });
  },
});

export const selectViewSeatsErrorMessage = (state: RootState) =>
  state.seats.errorMessage;
export const selectViewSeats = (state: RootState) => state.seats.allSeats;
export const selectViewAvailableSeats = (state: RootState) =>
  state.seats.availableSeats;
export const selectTripDetails = (state: RootState) => state.seats.trip;
export const selectSeatIsLoading = (state: RootState) => state.seats.status;
export const selectSelectedSeats = (state: RootState) =>
  state.seats.selectedSeats;
export const selectSeatsApproved = (state: RootState) =>
  state.seats.seatsApproved;

export const { selectedSeats, clearSeats } = seatSlice.actions;

export default seatSlice.reducer;
