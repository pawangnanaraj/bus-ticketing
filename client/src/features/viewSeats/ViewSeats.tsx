import React, { Fragment } from 'react';
import './viewSeats.css';
import { authAsync } from '../user/authSlice';
import AirlineSeatFlatIcon from '@mui/icons-material/AirlineSeatFlat';
import AirlineSeatFlatOutlinedIcon from '@mui/icons-material/AirlineSeatFlatOutlined';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import {
  selectViewSeats,
  selectTripDetails,
  selectSeatIsLoading,
  viewSeatAsync,
  selectedSeats,
  selectSelectedSeats,
  updateSeatStateAsync,
  selectViewSeatsErrorMessage,
  selectSeatsApproved,
} from './viewSeatsSlice';
import { setSeatId, setGender, setPassengerName } from '../ticket/ticketSlice';
import { Navigate } from 'react-router-dom';
import Spinner from '../../utils/Spinner';
import { selectSelectedTrip } from '../landing/viewTripsSlice';
import { selectIsAuthenticated } from '../user/authSlice';
import { format, parseISO } from 'date-fns';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import { Alert } from '@mui/material';

interface Seat {
  status: string;
  busId: string;
  tripId: string;
  seatNumber: number;
  passengerName?: string;
  _id: string;
}

export default function ViewSeats() {
  // const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const allSeats: Seat[] = useAppSelector(selectViewSeats);
  const selectTrip = useAppSelector(selectSelectedTrip);
  const errorMessage = useAppSelector(selectViewSeatsErrorMessage);
  const isAuthenticated = useAppSelector(selectIsAuthenticated);
  const selectSeat = useAppSelector(selectSelectedSeats);

  if (allSeats.length < 1) {
    const tripId = localStorage.getItem('tripId')
      ? localStorage.getItem('tripId')
      : selectTrip;
    dispatch(viewSeatAsync(tripId));
  }
  dispatch(authAsync(localStorage.token));
  const tripDetail = useAppSelector(selectTripDetails);
  const isLoading = useAppSelector(selectSeatIsLoading);
  const seatsApproved = useAppSelector(selectSeatsApproved);

  const renderTripHeading = () => {
    return (
      <Fragment>
        <h1>{tripDetail.busName}</h1>
        <h3>
          {tripDetail.fromCity} to {tripDetail.toCity} <br></br>
          <br></br>
          {format(parseISO(tripDetail.travelStartDate), 'dd-MMM-yyyy')}
          &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
          {format(parseISO(tripDetail.reachingDate), 'dd-MMM-yyyy')}
          <br></br>
          {tripDetail.travelStartTime}{' '}
          &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
          {tripDetail.reachingTime}
        </h3>
      </Fragment>
    );
  };

  const seatClass = (seatId: string) => {
    let a = 0;
    for (let i = 0; i < selectSeat.length; i++) {
      if (selectSeat[i].seatId === seatId) {
        console.log('selected');
        a = 1;
      }
    }
    if (a === 0) {
      return 'standard-button';
    } else return 'selected-seat';
  };

  if (seatsApproved) {
    return <Navigate to='/bookTicket' />;
  }

  const handleBookNow = (e: any) => {
    const seatIds = [];
    for (let i = 1; i < selectSeat.length; i++) {
      seatIds.push(selectSeat[i].seatId);
    }
    dispatch(updateSeatStateAsync({ seatId: seatIds, status: 'hold' }));
  };

  const bookNowButton = () => {
    return (
      <div>
        {/* errorMessage ? <div>{ErrorAlertFragment}</div> : ({' '} */}
        <button className='bookNow-button' onClick={(e) => handleBookNow(e)}>
          Confirm Selection
        </button>
      </div>
    );
  };

  const handleNameChange = (e: any, seatId: string) => {
    const passengerDetails = {
      passengerName: e.target.value,
      seatId: seatId,
    };
    dispatch(setPassengerName(passengerDetails));
  };

  const handleGenderChange = (e: any, seatId: string) => {
    const passengerDetails = {
      gender: e.target.value,
      seatId: seatId,
    };
    dispatch(setGender(passengerDetails));
  };

  const ErrorAlertFragment = errorMessage ? (
    <Fragment>
      <Alert severity='error'>
        <strong>
          {errorMessage[0][0]}{' '}
          {errorMessage[0][1] ? `and ${errorMessage[0][1]}` : ''}
        </strong>
      </Alert>
    </Fragment>
  ) : (
    ''
  );

  const selectSeatNumbers = () => {
    return (
      <Fragment>
        <p className='selected-seats'>
          {selectSeat.map((a) =>
            a.seatNumber === 0 ? (
              ''
            ) : (
              <div className='ticket-details'>
                {' '}
                <p>
                  <br></br>
                  <strong>Seat #: {a.seatNumber} </strong>
                </p>
                <FormControl component='fieldset'>
                  <TextField
                    label='Name'
                    helperText='Passenger Name'
                    variant='standard'
                    onChange={(e) => handleNameChange(e, a.seatId)}
                  />
                  <RadioGroup
                    row
                    name='row-radio-buttons-group'
                    onChange={(e) => handleGenderChange(e, a.seatId)}
                  >
                    <FormControlLabel
                      value='Female'
                      control={<Radio size='small' />}
                      label='Female'
                    />
                    <FormControlLabel
                      value='Male'
                      control={<Radio size='small' />}
                      label='Male'
                    />
                    <FormControlLabel
                      value='Other'
                      control={<Radio size='small' />}
                      label='Other'
                    />
                  </RadioGroup>
                </FormControl>
                {/* {dispatch(setSeatId(a.seatId))} */}
              </div>
            )
          )}
        </p>
        <strong>
          {' '}
          Total Price : {700 * (selectSeat.length - 1)} {'   '}
        </strong>
        {bookNowButton()}
      </Fragment>
    );
  };

  const handleClick = (
    seatId: string,
    seatNumber: number /* status: string*/
  ) => {
    const seatDetails = {
      seatId: seatId,
      seatNumber: seatNumber,
    };

    const passengerDetails = {
      passengerName: '',
      gender: '',
      seatId: seatId,
      seatNumber: seatNumber,
    };

    dispatch(selectedSeats(seatDetails));
    dispatch(setSeatId(passengerDetails));

    // dispatch(updateSeatStateAsync(seatId));
  };

  const renderSeatsFunction = () => {
    return allSeats.map((seat: Seat) => {
      return (
        <div key={seat._id}>
          {seat.status === 'available' ? (
            <div className='seat-item'>
              <button
                className={seatClass(seat._id)}
                value={seat._id}
                id={seat.seatNumber.toString()}
                name={seat._id}
                title={seat.seatNumber.toString()}
                onClick={() => handleClick(seat._id, seat.seatNumber)}
              >
                <AirlineSeatFlatOutlinedIcon style={{ fontSize: 40 }} />
              </button>
            </div>
          ) : (
            <div className='seat-item'>
              <button className='standard-unavailablebutton'>
                <AirlineSeatFlatIcon style={{ fontSize: 40 }} />
              </button>
            </div>
          )}
        </div>
      );
    });
  };

  return (
    <Fragment>
      {isAuthenticated ? (
        <div className='seat_inner'>
          {isLoading === 'loading' ? (
            <Spinner />
          ) : (
            <div>
              {renderTripHeading()}
              {ErrorAlertFragment}
              <div className='seat-container'>{renderSeatsFunction()}</div>
              {selectSeat.length > 1 ? (
                selectSeatNumbers()
              ) : (
                <p className='select-seats'>
                  <strong>Select Seats to continue booking</strong>
                </p>
              )}
            </div>
          )}
        </div>
      ) : (
        <Navigate to='/login' />
      )}
    </Fragment>
  );
}
