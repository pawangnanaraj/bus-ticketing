import React, { Fragment } from 'react';
import { selectMyTicket } from './viewTicketSplice';
import { format, parseISO } from 'date-fns';
import { useAppSelector } from '../../app/hooks';

// interface Ticket {
//   cancelled: boolean;
//   userId: string;
//   tripId: string;
//   busName: string;
//   fromCity: string;
//   toCity: string;
//   travelStartDate: string;
//   travelStartTime: string;
//   reachingDate: string;
//   reachingTime: string;
//   passengerDetails: [
//     {
//       passengerName: string;
//       seatNumber: number;
//       seatId: string;
//     }
//   ];
//   totalPrice: number;
//   emailId: string;
// }

export default function ViewTicket() {
  const myTicket = useAppSelector(selectMyTicket);

  const renderTicketHeading = () => {
    return (
      <div>
        <h1>{myTicket[0].busName}</h1>
        <h3>
          {myTicket[0].fromCity} to {myTicket[0].toCity} <br></br>
          <br></br>
          {/* {format(parseISO(myTicket[0].travelStartDate), 'dd-MMM-yyyy')} */}
          {myTicket[0].travelStartDate}
          &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
          {/* {format(parseISO(myTicket[0].reachingDate), 'dd-MMM-yyyy')} */}
          {myTicket[0].reachingDate}
          <br></br>
          {myTicket[0].travelStartTime}{' '}
          &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
          {myTicket[0].reachingTime}
        </h3>
      </div>
    );
  };

  const passengerData = myTicket[0].passengerDetails.map((value) => (
    <tr>
      <td>{value.passengerName}</td>
      <td>{value.gender}</td>
      <td>{value.seatNumber}</td>
    </tr>
  ));

  const displayTicket = () => {
    return (
      <table className='ticket-table'>
        <thead>
          <th>Passenger Name</th>
          <th>Gender</th>
          <th>Seat Number</th>
        </thead>
        <tbody>{passengerData}</tbody>
      </table>
    );
  };

  return (
    <div className='ticket-inner'>
      {renderTicketHeading()}
      {displayTicket()}
      <br></br>
      Email Id: {myTicket[0].emailId}
      <br></br>
      <br></br>
      Price: ₹ {myTicket[0].totalPrice}
      <br></br>
      <br></br>
    </div>
  );
}
