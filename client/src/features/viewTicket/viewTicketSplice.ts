import {
  createSlice,
  createAsyncThunk,
  PayloadAction,
  Slice,
} from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import api from '../../utils/api.utils';

export const viewTicketAsync = createAsyncThunk(
  '/myticket',
  async (ticketId: string, thunkAPI) => {
    try {
      const ticketDetails = api.get(`/ticket/me/${ticketId}`);
      return ticketDetails;
    } catch (error: any) {
      const errors = error.response.data;
      return thunkAPI.rejectWithValue(errors);
    }
  }
);

export interface viewTicketState {
  myTicket: [
    {
      _id: string;
      tripId: string;
      fromCity: string;
      toCity: string;
      busName: string;
      travelStartDate: string;
      travelStartTime: string;
      reachingDate: string;
      reachingTime: string;
      passengerDetails: [
        {
          passengerName: string;
          seatNumber: number;
          gender: string;
        }
      ];
      totalPrice: number;
      emailId: string;
    }
  ];

  isError: Boolean;
  errorMessage: [''] | null;
  status: 'loading' | 'idle' | 'failed';
  viewTicketApproved: Boolean;
}

const initialState: viewTicketState = {
  myTicket: [
    {
      _id: '',
      tripId: '',
      busName: '',
      fromCity: '',
      toCity: '',
      travelStartDate: '',
      travelStartTime: '',
      reachingDate: '',
      reachingTime: '',
      passengerDetails: [
        {
          passengerName: '',
          seatNumber: 0,
          gender: '',
        },
      ],
      totalPrice: 0,
      emailId: '',
    },
  ],

  isError: false,
  errorMessage: null,
  status: 'idle',
  viewTicketApproved: false,
};

export const viewMyTicketSlice: Slice<viewTicketState> = createSlice({
  name: 'viewMyTickets',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(viewTicketAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(
        viewTicketAsync.fulfilled,
        (state, action: PayloadAction<any>) => {
          state.status = 'idle';
          state.viewTicketApproved = true;
          state.myTicket[0] = action.payload.data;
        }
      )
      .addCase(
        viewTicketAsync.rejected,
        (state, action: PayloadAction<any>) => {
          state.status = 'failed';
          state.isError = true;
          state.errorMessage = action.payload.errors
            ? [action.payload.errors]
            : null;
        }
      );
  },
});

export const selectMyTicket = (state: RootState) => state.viewMyTicket.myTicket;
export const selectViewTicketApproved = (state: RootState) =>
  state.viewMyTicket.viewTicketApproved;
export const selectTicketStatus = (state: RootState) =>
  state.viewMyTicket.status;

export default viewMyTicketSlice.reducer;
