import React, { Fragment } from 'react';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import { Button } from '@mui/material';
import Stack from '@mui/material/Stack';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import {
  selectTrips,
  selectSearchTripsErrorMessage,
  selectSearchTripsLoading,
  selectedTrip,
} from './viewTripsSlice';
import { Alert } from '@mui/material';
import Spinner from '../../utils/Spinner';
import { useNavigate } from 'react-router-dom';
import { availableSeatsAsync, clearSeats } from '../viewSeats/viewSeatsSlice';

interface Trip {
  cancelled: boolean;
  busName: string;
  busId: string;
  _id?: string;
  agentId: object;
  busType: string;
  fromCity: string;
  toCity: string;
  totalSeats: number;
  availableSeats: number;
  pricePerSeat: number;
  travelStartDate: string;
  travelStartTime: string;
  reachingDate: string;
  reachingTime: string;
}

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(2),
  // textAlign: 'center',
  color: '#12824C',
  backgroundColor: '#FFFFFF',
  fontSize: 18,
  // border: 'none',
  // outline: 'none',
  borderRadius: 1,
}));

export default function BusGrid() {
  let navigate = useNavigate();
  const dispatch = useAppDispatch();
  const trips: Trip[] = useAppSelector(selectTrips);
  const loading: any = useAppSelector(selectSearchTripsLoading);
  const errorMessage: any = useAppSelector(selectSearchTripsErrorMessage);

  const handleClick = (tId: any) => {
    if (localStorage.getItem('tripId') !== tId) {
      localStorage.removeItem('tripId');
    }
    localStorage.setItem('tripId', tId);
    dispatch(clearSeats(tId));
    dispatch(selectedTrip(tId));
    dispatch(availableSeatsAsync(tId));
    navigate('/viewSeat');
  };

  const ErrorAlertFragment =
    loading === 'failed' ? (
      <Fragment>
        <br></br>
        <Alert severity='error'>
          <strong>
            {errorMessage[0][0]}{' '}
            {errorMessage[0][1] ? `and ${errorMessage[0][1]}` : ''}
            {errorMessage[0][2] ? `and ${errorMessage[0][2]}` : ''}
          </strong>
        </Alert>
      </Fragment>
    ) : (
      ''
    );

  const renderFunction = () => {
    if (trips.length > 0) {
      return trips.map((trip: Trip) => {
        return (
          <Fragment>
            <Stack
              direction={{ xs: 'column', sm: 'row' }}
              mt={1.5}
              maxHeight={{ sm: 220 }}
              style={{
                backgroundColor: '#FFFFFF',
                color: '#12824C',
                opacity: 0.9,
              }}
            >
              <Grid
                item
                container
                direction={'column'}
                key={trip._id}
                spacing={0}
              >
                <Grid item>
                  <Item>
                    <h3>{trip.busName}</h3>
                    {trip.busType}
                  </Item>
                </Grid>
              </Grid>
              <Grid item container direction={'column'} borderColor={'#12824C'}>
                {/* <Grid item>
          <Item>From City</Item>
        </Grid> */}
                <Grid item>
                  <Item>
                    From<br></br>
                    <strong>{trip.fromCity}</strong>
                    <br></br>
                    <br></br>
                    Departure at <br></br>
                    <strong>{trip.travelStartTime}</strong>
                  </Item>
                </Grid>
              </Grid>
              <Grid item container direction={'column'}>
                {/* <Grid item>
          <Item>To City</Item>
        </Grid> */}
                <Grid item>
                  <Item>
                    To<br></br>
                    <strong>{trip.toCity}</strong>
                    <br></br>
                    <br></br>Reaching at<br></br>
                    <strong>{trip.reachingTime}</strong>
                  </Item>
                </Grid>
              </Grid>
              <Grid item container direction={'column'} mt={1}>
                <Grid item>
                  <Item>
                    Available: <strong>{trip.availableSeats}</strong>
                    <br></br> ₹ <strong>{trip.pricePerSeat}</strong>/Seat
                  </Item>
                </Grid>
                <Grid item>
                  <Item>
                    <Button
                      variant='contained'
                      style={{ backgroundColor: '#272728', color: '#FFFFFF' }}
                      onClick={() => handleClick(trip._id)}
                    >
                      View Seats
                    </Button>
                  </Item>
                </Grid>
              </Grid>
            </Stack>
          </Fragment>
        );
      });
    }
  };
  return (
    <div>
      {ErrorAlertFragment}
      {loading === 'pending' ? (
        <Fragment>
          <Spinner />
        </Fragment>
      ) : (
        renderFunction()
      )}
    </div>
  );
}
