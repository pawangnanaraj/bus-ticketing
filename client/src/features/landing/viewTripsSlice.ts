import {
  createSlice,
  createAsyncThunk,
  PayloadAction,
  Slice,
} from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import axios from 'axios';

export const searchTripsAsync = createAsyncThunk(
  '/searchTrips',
  async (
    body: { fromCity: string; toCity: string; travelDate: string },
    thunkAPI
  ) => {
    try {
      const { fromCity, toCity, travelDate } = body;
      const trips = await axios({
        baseURL: '/api',
        method: 'get',
        url: '/trip/trips',
        headers: {
          'Content-Type': 'application/json',
        },
        params: { fromCity, toCity, travelDate },
      });

      return trips;
    } catch (error: any) {
      const errors = error.response.data;
      return thunkAPI.rejectWithValue(errors);
    }
  }
);

export interface TripsState {
  trips: [];
  status: 'idle' | 'loading' | 'failed';
  selectedTrip: any;
  isError: Boolean;
  errorMessage: any;
}

const initialState: TripsState = {
  trips: [],
  status: 'idle',
  selectedTrip: '',
  isError: false,
  errorMessage: [''],
};

export const tripsSlice: Slice<TripsState> = createSlice({
  name: 'trips',
  initialState,

  reducers: {
    selectedTrip: (state, action: PayloadAction<any>) => {
      state.selectedTrip = action.payload;
    },
    clearTrips: (state, action: PayloadAction<any>) => {
      state.trips = [];
      state.selectedTrip = '';
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(
        searchTripsAsync.fulfilled,
        (state, action: PayloadAction<any>) => {
          state.status = 'idle';
          state.trips = action.payload.data;
          state.isError = false;
          state.errorMessage = null;
        }
      )
      .addCase(searchTripsAsync.pending, (state) => {
        state.status = 'loading';
        state.isError = false;
        state.errorMessage = null;
      })
      .addCase(
        searchTripsAsync.rejected,
        (state, action: PayloadAction<any>) => {
          state.status = 'failed';
          state.isError = false;
          state.trips = [];
          state.errorMessage = [action.payload.errors];
        }
      );
  },
});

export const selectTrips = (state: RootState) => state.trips.trips;
export const selectSearchTripsIsError = (state: RootState) =>
  state.trips.isError;
export const selectSearchTripsErrorMessage = (state: RootState) =>
  state.trips.errorMessage;
export const selectSearchTripsLoading = (state: RootState) =>
  state.trips.status;
export const selectSelectedTrip = (state: RootState) =>
  state.trips.selectedTrip;

export const { selectedTrip, clearTrips } = tripsSlice.actions;

export default tripsSlice.reducer;
