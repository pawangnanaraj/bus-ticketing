import React from 'react';
import SearchIcon from '@mui/icons-material/Search';
import Button from '@mui/material/Button';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import {
  selectSelectedFromCity,
  selectSelectedToCity,
  selectTravelDate,
} from './viewLocationSlice';
import { searchTripsAsync, clearTrips } from './viewTripsSlice';

export default function SearchTripsButton() {
  const dispatch = useAppDispatch();
  const fromCity = useAppSelector(selectSelectedFromCity);
  const toCity = useAppSelector(selectSelectedToCity);
  const travelDate = useAppSelector(selectTravelDate);

  const HandleSubmit = async (e: any) => {
    dispatch(clearTrips('any'));
    dispatch(searchTripsAsync({ fromCity, toCity, travelDate }));
  };

  return (
    <Button
      variant='contained'
      style={{ backgroundColor: '#12824C', color: '#FFFFFF' }}
      startIcon={<SearchIcon />}
      onClick={HandleSubmit}
    >
      {' '}
      Search Bus
    </Button>
  );
}
