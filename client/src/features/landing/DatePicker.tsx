import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import { selectTravelDate, setTravelDate } from './viewLocationSlice';
import { format } from 'date-fns';

export default function TravelDatePicker() {
  const dispatch = useAppDispatch();
  const travelDate = useAppSelector(selectTravelDate);

  return (
    <Box
      borderRadius={2}
      style={{
        backgroundColor: '#FFFFFF',
        color: '#12824C',
      }}
    >
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <DatePicker
          label='Travel Date'
          disablePast
          value={travelDate}
          onChange={(newValue) => {
            dispatch(setTravelDate(format(newValue, 'yyyy-MM-dd')));
          }}
          renderInput={(params) => <TextField {...params} />}
        />
      </LocalizationProvider>
    </Box>
  );
}
