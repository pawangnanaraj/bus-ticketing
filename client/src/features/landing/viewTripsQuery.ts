import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const viewTripsApi = createApi({
  reducerPath: 'viewTripsApi',
  baseQuery: fetchBaseQuery({ baseUrl: '/api/trip/trips' }),
  endpoints: (builder) => ({
    viewTrips: builder.mutation({
      query: (data) => ({
        url: `/`,
        method: 'PUT',
        data,
      }),
    }),
  }),
});

// Export hooks for usage in functional components
export const { useViewTripsMutation } = viewTripsApi;
