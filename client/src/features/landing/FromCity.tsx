import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import {
  selectFromCity,
  selectSelectedFromCity,
  setSelectedFromCity,
} from './viewLocationSlice';

export default function FromCity() {
  const dispatch = useAppDispatch();
  const fromCityData: string[] = useAppSelector(selectFromCity);
  const selectedFromCityData = useAppSelector(selectSelectedFromCity);

  const handleChange = (event: SelectChangeEvent) => {
    dispatch(setSelectedFromCity(event.target.value as string));
  };

  return (
    <Box
      borderRadius={2}
      style={{
        backgroundColor: '#FFFFFF',
        color: '#12824C',
      }}
    >
      <FormControl
        sx={{
          minWidth: 150,
          maxHeight: 40,
          color: '#12824C',
        }}
      >
        <InputLabel
          id='demo-simple-select-label'
          style={{
            backgroundColor: '#FFFFFF',
            color: '#12824C',
          }}
        >
          From City
        </InputLabel>
        <Select
          labelId='demo-simple-select-label'
          id='demo-simple-select'
          value={selectedFromCityData}
          label='Age'
          autoWidth
          onChange={handleChange}
        >
          {fromCityData.map((city: string) => (
            <MenuItem key={city} value={city}>
              {city}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
}
