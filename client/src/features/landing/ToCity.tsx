import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import {
  selectToCity,
  selectSelectedToCity,
  setSelectedToCity,
} from './viewLocationSlice';

export default function ToCity() {
  const dispatch = useAppDispatch();
  const toCityData: string[] = useAppSelector(selectToCity);
  const selectedToCityData = useAppSelector(selectSelectedToCity);

  const handleChange = (event: SelectChangeEvent) => {
    dispatch(setSelectedToCity(event.target.value as string));
  };

  return (
    <Box
      borderRadius={2}
      borderColor={'unset'}
      style={{
        backgroundColor: '#FFFFFF',
        color: '#12824C',
      }}
    >
      <FormControl
        sx={{
          minWidth: 150,
          maxHeight: 40,
          color: '#12824C',
        }}
      >
        <InputLabel
          id='demo-simple-select-label'
          style={{
            backgroundColor: '#FFFFFF',
            color: '#12824C',
          }}
        >
          Destination
        </InputLabel>
        <Select
          labelId='demo-simple-select-label'
          id='demo-simple-select'
          value={selectedToCityData}
          label='Age'
          autoWidth
          onChange={handleChange}
        >
          {/* {displayFromCity} */}
          {toCityData.map((city: string) => (
            <MenuItem key={city} value={city}>
              {city}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
}
