import React from 'react';
import styles from './Landing.module.css';
import Stack from '@mui/material/Stack';
import FromCity from './FromCity';
import ToCity from './ToCity';
import TravelDatePicker from './DatePicker';
import { viewCitiesAsync } from './viewLocationSlice';
import { useAppDispatch } from '../../app/hooks';
import SearchTripsButton from './SearchTrips';
import BusGrid from './ViewTripsGrid';

export function Landing() {
  const dispatch = useAppDispatch();
  dispatch(viewCitiesAsync());
  return (
    <section className={styles.dark_overlay}>
      <div className={styles.landing_inner}>
        <h1 className={styles.x_large}>Paw Bus</h1>
        <p className={styles.lead}>Book your buses here to travel the world</p>
        <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2}>
          <FromCity />
          <ToCity />
          <TravelDatePicker />
          <SearchTripsButton />
        </Stack>
        <BusGrid />
      </div>
    </section>
  );
}
