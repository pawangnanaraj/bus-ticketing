import {
  createSlice,
  createAsyncThunk,
  PayloadAction,
  Slice,
} from '@reduxjs/toolkit';
import api from '../../utils/api.utils';
import { RootState } from '../../app/store';
import { format } from 'date-fns';

export const viewCitiesAsync = createAsyncThunk(
  '/fromCity',
  async (thunkAPI) => {
    try {
      const Cities = await api.get('/trip');
      return Cities;
    } catch (error: any) {}
  }
);

export interface CitiesState {
  fromCity: string[];
  toCity: string[];
  selectedFromCity: string;
  selectedToCity: string;
  travelDate: string;
  status: 'idle' | 'loading' | 'failed';
  isError: Boolean;
  errorMessage: any;
}

const initialState: CitiesState = {
  fromCity: [''],
  toCity: [''],
  selectedFromCity: '',
  selectedToCity: '',
  travelDate: format(new Date(), 'yyyy-MM-dd'),
  status: 'idle',
  isError: false,
  errorMessage: null,
};

export const citySlice: Slice = createSlice({
  name: 'cities',
  initialState,

  reducers: {
    setFromCity: (state, action) => {
      state.fromCity = action.payload;
    },
    setToCity: (state, action) => {
      state.toCity = action.payload;
    },
    setTravelDate: (state, action) => {
      state.travelDate = action.payload;
    },
    setSelectedFromCity: (state, action) => {
      state.selectedFromCity = action.payload;
    },
    setSelectedToCity: (state, action: PayloadAction<string>) => {
      state.selectedToCity = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(
        viewCitiesAsync.fulfilled,
        (state, action: PayloadAction<any>) => {
          state.fromCity = action.payload.data.fromCity;
          state.toCity = action.payload.data.toCity;
          state.status = 'idle';
          state.isError = false;
          state.errorMessage = null;
        }
      )
      .addCase(viewCitiesAsync.pending, (state) => {
        state.status = 'loading';
        state.isError = false;
        state.errorMessage = null;
      })
      .addCase(
        viewCitiesAsync.rejected,
        (state, action: PayloadAction<any>) => {
          state.status = 'loading';
          state.isError = true;
          state.errorMessage = action.payload.errors
            ? [action.payload.errors]
            : null;
        }
      );
  },
});

export const selectFromCity = (state: RootState) => state.city.fromCity;
export const selectSelectedFromCity = (state: RootState) =>
  state.city.selectedFromCity;
export const selectTravelDate = (state: RootState) => state.city.travelDate;
export const selectToCity = (state: RootState) => state.city.toCity;
export const selectSelectedToCity = (state: RootState) =>
  state.city.selectedToCity;
export const selectViewCitiesIsError = (state: RootState) => state.city.isError;
export const selectViewCitiesErrorMessage = (state: RootState) =>
  state.city.errorMessage;

export const {
  setFromCity,
  setSelectedFromCity,
  setToCity,
  setSelectedToCity,
  setTravelDate,
} = citySlice.actions;

export default citySlice.reducer;
