import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import styles from './navbar.module.css';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import { selectIsAuthenticated } from '../user/authSlice';
import { logOut } from '../user/authSlice';

const NavBar = () => {
  const dispatch = useAppDispatch();
  const isAuthenticated = useAppSelector(selectIsAuthenticated);

  const onClick = (e: any) => {
    dispatch(logOut(e));
  };

  const guestLinks = (
    <ul className={styles.navbar_ul}>
      <li>
        <Link className={styles.navbar_li} to='/register'>
          Sign Up
        </Link>
      </li>
      <li>
        <Link className={styles.navbar_li} to='/login'>
          Login
        </Link>
      </li>
    </ul>
  );

  const authLinks = (
    <ul className={styles.navbar_ul}>
      <li>
        <Link className={styles.navbar_li} to='/myBookings'>
          <i className='fa fa-plane' />{' '}
          <span className='hide-sm'>My Trips</span>
        </Link>
      </li>
      <li>
        <Link className={styles.navbar_li} onClick={onClick} to='/'>
          <i className='fas fa-sign-out-alt' />{' '}
          <span className='hide-sm'>Logout</span>
        </Link>
      </li>
    </ul>
  );

  return (
    <nav className={styles.navbar}>
      <h1>
        <Link className={styles.navbar_li} to='/'>
          <i className='fa fa-bus' /> Home
        </Link>
      </h1>
      <Fragment>{isAuthenticated ? authLinks : guestLinks}</Fragment>
    </nav>
  );
};

export default NavBar;
