import React, { Fragment, useState } from 'react';
import { Link, Navigate } from 'react-router-dom';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye } from '@fortawesome/free-solid-svg-icons';
import styles from './user.module.css';
import {
  loginAsync,
  selectEmail,
  selectPassword,
  selectIsAuthenticated,
  selectErrorMessage,
} from './authSlice';
import { setEmail, setPassword } from './authSlice';
import { Alert } from '@mui/material';

const eye = <FontAwesomeIcon icon={faEye} />;

const LoginUser = () => {
  const dispatch = useAppDispatch();
  const email = useAppSelector(selectEmail);
  const password = useAppSelector(selectPassword);
  const isAuthenticated = useAppSelector(selectIsAuthenticated);
  const errorMessage = useAppSelector(selectErrorMessage);

  const onChangeEmail = (e: any) => {
    dispatch(setEmail(e.target.value));
  };

  const onChangePassword = (e: any) => {
    dispatch(setPassword(e.target.value));
  };

  const onSubmit = (e: any) => {
    e.preventDefault();
    const body = { email, password };
    dispatch(loginAsync(body));
  };

  const [passwordShown, setPasswordShown] = useState(false);

  const togglePasswordVisibility = () => {
    setPasswordShown(passwordShown ? false : true);
  };

  if (isAuthenticated) {
    return <Navigate to='/' />;
  }

  const ErrorAlertFragment = errorMessage ? (
    <Fragment>
      <Alert severity='error'>
        <strong>
          {errorMessage[0][0]}{' '}
          {errorMessage[0][1] ? `and ${errorMessage[0][1]}` : ''}
        </strong>
      </Alert>
    </Fragment>
  ) : (
    ''
  );

  return (
    <section className={styles.container}>
      {ErrorAlertFragment}
      <h1 className={styles.large}>Log In</h1>
      <p className={styles.lead}>
        <i className='fas fa-user' /> Please log in to book your ticket
      </p>
      <form className={styles.form} onSubmit={onSubmit}>
        <div className={styles.form_group}>
          <input
            className={styles.form_textarea}
            type='email'
            placeholder='Email Address'
            name='email'
            value={email}
            onChange={onChangeEmail}
            // required
          />
        </div>
        <div className={styles.form_group}>
          <input
            type={passwordShown ? 'text' : 'password'}
            placeholder='Password'
            name='password'
            value={password}
            onInput={onChangePassword}
            // minLength = '6'
          />
          <i
            className={styles.form_textarea_i}
            onClick={togglePasswordVisibility}
          >
            {eye}
          </i>
        </div>

        <input type='submit' className={styles.btn_primary} value='Sign In' />
      </form>
      <p className='my-1'>
        Don't have an account? <Link to='/register'>Register</Link>
      </p>
    </section>
  );
};

export default LoginUser;
