import {
  createSlice,
  createAsyncThunk,
  PayloadAction,
  Slice,
} from '@reduxjs/toolkit';
import api from '../../utils/api.utils';
import { RootState } from '../../app/store';
import setAuthToken from '../../utils/setAuthToken';

export const authAsync = createAsyncThunk(
  '/auth',
  async (token: string, thunkAPI) => {
    if (localStorage.token) {
      setAuthToken(token);
    }
    try {
      const res = await api.get('/auth');
      return res;
    } catch (error: any) {
      localStorage.removeItem('token');
      const errors = error.response.data;
      return thunkAPI.rejectWithValue(errors);
    }
  }
);

export const loginAsync = createAsyncThunk(
  '/auth/login',
  async (body: { email: string; password: string }, thunkAPI) => {
    try {
      const res = await api.post('/auth', body);
      localStorage.setItem('token', res.data.token);
      return res.data;
    } catch (error: any) {
      localStorage.removeItem('token');
      const errors = error.response.data;
      return thunkAPI.rejectWithValue(errors);
    }
  }
);

export const registerAsync = createAsyncThunk(
  '/register',
  async (
    body: { userName: string; email: string; password: string },
    thunkAPI
  ) => {
    try {
      const res = await api.post('/register', body);

      localStorage.setItem('token', res.data.token);

      return res.data;
    } catch (error: any) {
      localStorage.removeItem('token');
      const errors = error.response.data;
      return thunkAPI.rejectWithValue(errors);
    }
  }
);

export interface AuthState {
  isAuthenticated: true | false | null;
  status: 'idle' | 'loading' | 'failed';
  user: {
    _id: '';
    userName: '';
    email: '';
    isAgent: '';
    password: '';
    password2: '';
  };
  token: string | null;
  isError: Boolean;
  errorMessage: [''] | null;
}

const initialState: AuthState = {
  isAuthenticated: null,
  status: 'idle',
  user: {
    _id: '',
    userName: '',
    email: '',
    isAgent: '',
    password: '',
    password2: '',
  },
  token: '',
  isError: false,
  errorMessage: null,
};

export const authSlice: Slice<AuthState> = createSlice({
  name: 'auth',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    logOut: (state) => {
      localStorage.removeItem('token');
      state.isAuthenticated = null;
      state.status = 'idle';
      state.user = {
        _id: '',
        userName: '',
        email: '',
        isAgent: '',
        password: '',
        password2: '',
      };
      state.token = '';
      state.isError = false;
      state.errorMessage = null;
    },
    setEmail: (state, action) => {
      state.status = 'loading';
      state.errorMessage = null;
      state.isError = false;
      state.isAuthenticated = false;
      state.token = null;
      state.user = { ...state.user, email: action.payload };
    },
    setPassword: (state, action) => {
      state.status = 'loading';
      state.errorMessage = null;
      state.isError = false;
      state.isAuthenticated = false;
      state.token = null;
      state.user = { ...state.user, password: action.payload };
    },
    setPassword2: (state, action) => {
      state.status = 'loading';
      state.errorMessage = null;
      state.isError = false;
      state.isAuthenticated = false;
      state.token = null;
      state.user = { ...state.user, password2: action.payload };
    },
    setUserName: (state, action: PayloadAction<any>) => {
      state.status = 'loading';
      state.errorMessage = null;
      state.isError = false;
      state.isAuthenticated = false;
      state.token = null;
      state.user = { ...state.user, userName: action.payload };
    },
  },
  // The `extraReducers` field lets the slice handle actions defined elsewhere,
  // including actions generated by createAsyncThunk or in other slices.
  extraReducers: (builder) => {
    builder
      .addCase(authAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(authAsync.fulfilled, (state, action: PayloadAction<any>) => {
        state.status = 'idle';
        state.user = action.payload.data;
        state.isAuthenticated = true;
        state.token = localStorage.getItem('token');
        state.errorMessage = null;
        state.isError = false;
      })
      .addCase(authAsync.rejected, (state, action: PayloadAction<any>) => {
        localStorage.removeItem('token');
        state.status = 'failed';
        state.user = {
          _id: '',
          userName: '',
          email: '',
          isAgent: '',
          password: '',
          password2: '',
        };
        state.isAuthenticated = false;
        state.token = null;
        state.isError = true;
        state.errorMessage = action.payload.errors
          ? [action.payload.errors]
          : null;
      })
      .addCase(loginAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(loginAsync.fulfilled, (state, action: PayloadAction<any>) => {
        state.status = 'idle';
        state.user = {
          _id: '',
          userName: '',
          email: '',
          isAgent: '',
          password: '',
          password2: '',
        };
        state.isAuthenticated = true;
        state.token = action.payload;
        state.errorMessage = null;
        state.isError = false;
      })
      .addCase(loginAsync.rejected, (state, action: PayloadAction<any>) => {
        localStorage.removeItem('token');
        state.status = 'failed';
        state.isAuthenticated = false;
        state.token = null;
        state.isError = true;
        state.errorMessage = [action.payload.errors];
      })
      .addCase(registerAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(registerAsync.fulfilled, (state, action: PayloadAction<any>) => {
        state.status = 'idle';
        state.user = {
          _id: '',
          userName: '',
          email: '',
          isAgent: '',
          password: '',
          password2: '',
        };
        state.isAuthenticated = true;
        state.token = action.payload;
        state.errorMessage = null;
        state.isError = false;
      })
      .addCase(registerAsync.rejected, (state, action: PayloadAction<any>) => {
        localStorage.removeItem('token');
        state.status = 'failed';
        state.isAuthenticated = false;
        state.token = null;
        state.isError = true;
        state.errorMessage = [action.payload.errors];
      });
  },
});

export const selectIsAuthenticated = (state: RootState) =>
  state.auth.isAuthenticated;

export const selectEmail = (state: RootState) => state.auth.user.email;
export const selectUserId = (state: RootState) => state.auth.user._id;
export const selectPassword = (state: RootState) => state.auth.user.password;
export const selectPassword2 = (state: RootState) => state.auth.user.password2;
export const selectUserName = (state: RootState) => state.auth.user.userName;
export const selectisError = (state: RootState) => state.auth.isError;
export const selectErrorMessage = (state: RootState) => state.auth.errorMessage;

export const { logOut, setEmail, setPassword, setPassword2, setUserName } =
  authSlice.actions;

export default authSlice.reducer;
