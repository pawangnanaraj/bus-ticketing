import React, { Fragment, useState } from 'react';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import { Link, Navigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye } from '@fortawesome/free-solid-svg-icons';
import styles from './user.module.css';
import {
  selectEmail,
  selectPassword,
  selectPassword2,
  selectUserName,
  setPassword2,
  setUserName,
  setPassword,
  setEmail,
  selectIsAuthenticated,
  selectErrorMessage,
} from './authSlice';
import { registerAsync } from './authSlice';
import { Alert } from '@mui/material';

const eye = <FontAwesomeIcon icon={faEye} />;

const RegisterUser = () => {
  const dispatch = useAppDispatch();
  const email = useAppSelector(selectEmail);
  const password = useAppSelector(selectPassword);
  const password2 = useAppSelector(selectPassword2);
  const userName = useAppSelector(selectUserName);
  const isAuthenticated = useAppSelector(selectIsAuthenticated);
  const errorMessage = useAppSelector(selectErrorMessage);

  const onChangeEmail = (e: any) => {
    dispatch(setEmail(e.target.value));
  };

  const onChangePassword = (e: any) => {
    dispatch(setPassword(e.target.value));
  };
  const onChangePassword2 = (e: any) => {
    dispatch(setPassword2(e.target.value));
  };
  const onChangeUserName = (e: any) => {
    dispatch(setUserName(e.target.value));
  };

  const onSubmit = (e: any) => {
    e.preventDefault();
    const body = { userName, email, password };
    dispatch(registerAsync(body));
  };

  const [passwordShown, setPasswordShown] = useState(false);

  const togglePasswordVisibility = () => {
    setPasswordShown(passwordShown ? false : true);
  };
  if (isAuthenticated) {
    return <Navigate to='/' />;
  }

  const alertFragment = errorMessage ? (
    <Fragment>
      <Alert severity='error'>
        {errorMessage[0][0]}{' '}
        {errorMessage[0][1] ? `, ${errorMessage[0][1]}` : ''}
        {errorMessage[0][2] ? `, ${errorMessage[0][2]}` : ''}
        {errorMessage[0][3] ? `and ${errorMessage[0][3]}` : ''}
      </Alert>
    </Fragment>
  ) : (
    ''
  );

  return (
    <section className={styles.container}>
      {alertFragment}
      <h1 className={styles.large}>Sign Up</h1>
      <p className={styles.lead}>
        <i className='fas fa-user' /> Create Your Account
      </p>
      <form className={styles.form} onSubmit={onSubmit}>
        <div className={styles.form_group}>
          <input
            type='text'
            placeholder='Name'
            name='name'
            value={userName}
            onChange={onChangeUserName}
            // required
          />
        </div>
        <div className={styles.form_group}>
          <input
            className={styles.form_textarea}
            type='email'
            placeholder='Email Address'
            name='email'
            value={email}
            onChange={onChangeEmail}
            // required
          />
        </div>
        <div className={styles.form_group}>
          <input
            type={passwordShown ? 'text' : 'password'}
            placeholder='Password'
            name='password'
            value={password}
            onChange={onChangePassword}
            // minLength = '6'
          />
        </div>
        <div className={styles.form_group}>
          <input
            type={passwordShown ? 'text' : 'password'}
            placeholder='Confirm Password'
            name='password2'
            value={password2}
            onChange={onChangePassword2}
            // minLength = '6'
          />
          <i
            className={styles.form_textarea_i}
            onClick={togglePasswordVisibility}
          >
            {eye}
          </i>
        </div>
        <input type='submit' className={styles.btn_primary} value='Register' />
      </form>
      <p className='my-1'>
        Already have an account? <Link to='/login'>Sign In</Link>
      </p>
    </section>
  );
};

export default RegisterUser;
