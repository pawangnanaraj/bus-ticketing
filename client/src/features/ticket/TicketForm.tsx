import React, { Fragment } from 'react';
import { Navigate } from 'react-router-dom';
import './ticketForm.css';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import {
  selectSelectedSeats,
  selectTripDetails,
} from '../viewSeats/viewSeatsSlice';
import { format, parseISO } from 'date-fns';
import { selectSelectedTrip } from '../landing/viewTripsSlice';
import {
  selectTicketEmailId,
  selectPassengerDetails,
  selectTicketErrorMessage,
  setEmail,
  selectBookingSuccessful,
} from './ticketSlice';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import { Alert } from '@mui/material';
import { bookATicketAsync } from './ticketSlice';
import Ticket from './TicketInterface';
// export default interface Ticket {
//   passengerDetails: [
//     {
//       passengerName: string;
//       seatNumber: string;
//       seatId: string;
//     }
//   ];
//   totalPrice: number;
//   emailId: string;
// }

export default function TicketForm() {
  const dispatch = useAppDispatch();
  const selectedSeats = useAppSelector(selectSelectedSeats);
  const tripDetail = useAppSelector(selectTripDetails);
  const ticketEmailId = useAppSelector(selectTicketEmailId);
  const passengerDetails = useAppSelector(selectPassengerDetails);
  const ticketErrorMessage = useAppSelector(selectTicketErrorMessage);
  // const ticketIsError = useAppSelector(selectTicketIsError);
  const tripId: string = useAppSelector(selectSelectedTrip);
  const bookingSuccessful = useAppSelector(selectBookingSuccessful);

  console.log(selectedSeats);

  if (bookingSuccessful) {
    return <Navigate to='/myBookings' />;
  }

  const renderTicketHeading = () => {
    return (
      <Fragment>
        {' '}
        <h1>{tripDetail.busName}</h1>
        <h3>
          {tripDetail.fromCity} to {tripDetail.toCity} <br></br>
          <br></br>
          {format(parseISO(tripDetail.travelStartDate), 'dd-MMM-yyyy')}
          &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
          {format(parseISO(tripDetail.reachingDate), 'dd-MMM-yyyy')}
          <br></br>
          {tripDetail.travelStartTime}{' '}
          &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
          {tripDetail.reachingTime}
        </h3>
      </Fragment>
    );
  };

  const ErrorAlertFragment = ticketErrorMessage ? (
    <Fragment>
      <Alert severity='error'>
        <strong>
          {ticketErrorMessage[0][0]}{' '}
          {ticketErrorMessage[0][1] ? `and ${ticketErrorMessage[0][1]}` : ''}
        </strong>
      </Alert>
    </Fragment>
  ) : (
    ''
  );

  const passengerData = passengerDetails.map((value) => (
    <tr>
      <td>{value.passengerName}</td>
      <td>{value.gender}</td>
      <td>{value.seatNumber}</td>
    </tr>
  ));

  const displayPassengerDetails = () => {
    return (
      <Fragment>
        <table className='table'>
          <thead>
            <th>Passenger Name</th>
            <th>Gender</th>
            <th>Seat Number</th>
          </thead>
          <tbody>{passengerData}</tbody>
        </table>
      </Fragment>
    );
  };

  const handleBookNow = (e: any) => {
    const ticketDetails = {} as Ticket;
    console.log('passengerDetails', passengerDetails);
    console.log('passengerDetails', passengerDetails);
    ticketDetails.passengerDetails = passengerDetails;
    ticketDetails.emailId = ticketEmailId;
    ticketDetails.totalPrice = 700 * passengerDetails.length;
    dispatch(bookATicketAsync({ ticketDetails, tripId }));
    console.log(ticketDetails);
  };

  const bookNowButton = () => {
    return (
      <div>
        <button className='bookNow-button' onClick={(e) => handleBookNow(e)}>
          Book Ticket
        </button>
      </div>
    );
  };

  const handleEmailChange = (e: any) => {
    dispatch(setEmail(e.target.value));
  };

  return (
    <div className='ticket-inner'>
      <p>
        Do not refresh the page <br></br>
        <strong>Complete booking within 1 minute</strong>
      </p>
      {ErrorAlertFragment}
      {renderTicketHeading()}
      {displayPassengerDetails()}
      <br></br>
      <strong>
        {' '}
        Total Price : {700 * passengerDetails.length} {'   '}
      </strong>
      <br></br>
      <FormControl component='fieldset' size='medium'>
        <TextField
          label='Email Id'
          helperText='Please enter your Email Id'
          variant='standard'
          onChange={(e) => handleEmailChange(e)}
        />
      </FormControl>
      {bookNowButton()}
    </div>
  );
}
