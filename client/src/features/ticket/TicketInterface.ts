export default interface Ticket {
  passengerDetails: [
    {
      passengerName: string;
      seatNumber: string;
      seatId: string;
      gender: string;
    }
  ];
  totalPrice: number;
  emailId?: string;
}
