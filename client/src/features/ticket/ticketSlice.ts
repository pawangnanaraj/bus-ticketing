import {
  createSlice,
  createAsyncThunk,
  PayloadAction,
  Slice,
} from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import api from '../../utils/api.utils';
import Ticket from './TicketInterface';

export const bookATicketAsync = createAsyncThunk(
  '/bookTicket',
  async (ticket: { ticketDetails: Ticket; tripId: string }, thunkAPI) => {
    try {
      const bookedTicket = await api.post(
        `/ticket/book/${ticket.tripId}`,
        ticket.ticketDetails
      );
      return bookedTicket;
    } catch (error: any) {
      const errors = error.response.data;
      return thunkAPI.rejectWithValue(errors);
    }
  }
);

export interface ticketState {
  passengerDetails: [
    {
      passengerName: '';
      gender: '';
      seatId: '';
      seatNumber: '';
    }
  ];
  emailId: '';
  isError: Boolean;
  errorMessage: [''] | null;
  status: 'loading' | 'idle' | 'failed';
  bookingSuccessful: Boolean;
}

const initialState: ticketState = {
  passengerDetails: [
    {
      passengerName: '',
      gender: '',
      seatId: '',
      seatNumber: '',
    },
  ],
  emailId: '',
  isError: false,
  errorMessage: null,
  status: 'idle',
  bookingSuccessful: false,
};

export const ticketSlice: Slice<ticketState> = createSlice({
  name: 'ticket',
  initialState,
  reducers: {
    setSeatId: (state, action: PayloadAction<any>) => {
      let a = 0;
      for (let i = 0; i < state.passengerDetails.length; i++) {
        if (state.passengerDetails[i].seatId === action.payload.seatId) {
          state.passengerDetails.splice(i, 1);
          a = 1;
        }
        if (state.passengerDetails[i].seatId === '') {
          state.passengerDetails.splice(i, 1);
          a = 0;
        }
      }
      if (a === 0) {
        state.passengerDetails.push(action.payload);
      }
    },
    setPassengerName: (state, action: PayloadAction<any>) => {
      for (let i = 0; i < state.passengerDetails.length; i++) {
        if (state.passengerDetails[i].seatId === action.payload.seatId) {
          state.passengerDetails[i].passengerName =
            action.payload.passengerName;
        }
      }
    },
    setGender: (state, action: PayloadAction<any>) => {
      for (let i = 0; i < state.passengerDetails.length; i++) {
        if (state.passengerDetails[i].seatId === action.payload.seatId) {
          state.passengerDetails[i].gender = action.payload.gender;
        }
      }
    },
    setEmail: (state, action: PayloadAction<any>) => {
      state.emailId = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(bookATicketAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(
        bookATicketAsync.rejected,
        (state, action: PayloadAction<any>) => {
          state.status = 'failed';
          state.isError = true;
          state.errorMessage = action.payload.errors
            ? [action.payload.errors]
            : null;
        }
      )
      .addCase(
        bookATicketAsync.fulfilled,
        (state, action: PayloadAction<any>) => {
          state.status = 'idle';
          state.isError = false;
          state.bookingSuccessful = true;
        }
      );
  },
});

export const { setSeatId, setPassengerName, setGender, setEmail } =
  ticketSlice.actions;

export const selectPassengerDetails = (state: RootState) =>
  state.ticket.passengerDetails;
export const selectTicketEmailId = (state: RootState) => state.ticket.emailId;
export const selectTicketErrorMessage = (state: RootState) =>
  state.ticket.errorMessage;
export const selectTicketIsError = (state: RootState) => state.ticket.isError;
export const selectBookingSuccessful = (state: RootState) =>
  state.ticket.bookingSuccessful;

export default ticketSlice.reducer;
