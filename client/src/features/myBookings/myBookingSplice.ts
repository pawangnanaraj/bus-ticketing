import {
  createSlice,
  createAsyncThunk,
  PayloadAction,
  Slice,
} from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import api from '../../utils/api.utils';

export const myBookingsAsync = createAsyncThunk(
  '/myBookings',
  async (userId: string, thunkAPI) => {
    try {
      const myBookings = api.get('/ticket/me');
      return myBookings;
    } catch (error: any) {
      const errors = error.response.data;
      return thunkAPI.rejectWithValue(errors);
    }
  }
);

export interface BookingsState {
  myBookings: [
    {
      _id: string;
      tripId: string;
      fromCity: string;
      toCity: string;
      busName: string;
      travelStartDate: string;
      travelStartTime: string;
      reachingDate: string;
      reachingTime: string;
      passengerDetails: [
        {
          passengerName: string;
          seatNumber: number;
          gender: string;
        }
      ];
      totalPrice: number;
      emailId: string;
    }
  ];

  isError: Boolean;
  errorMessage: [''] | null;
  status: 'loading' | 'idle' | 'failed';
}

const initialState: BookingsState = {
  myBookings: [
    {
      _id: '',
      tripId: '',
      busName: '',
      fromCity: '',
      toCity: '',
      travelStartDate: '',
      travelStartTime: '',
      reachingDate: '',
      reachingTime: '',
      passengerDetails: [
        {
          passengerName: '',
          seatNumber: 0,
          gender: '',
        },
      ],
      totalPrice: 0,
      emailId: '',
    },
  ],

  isError: false,
  errorMessage: null,
  status: 'idle',
};

export const bookingSlice: Slice<BookingsState> = createSlice({
  name: 'bookings',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(myBookingsAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(
        myBookingsAsync.rejected,
        (state, action: PayloadAction<any>) => {
          state.status = 'failed';
          state.isError = true;
          state.errorMessage = action.payload.errors
            ? [action.payload.errors]
            : null;
        }
      )
      .addCase(
        myBookingsAsync.fulfilled,
        (state, action: PayloadAction<any>) => {
          state.status = 'idle';
          state.isError = false;
          state.myBookings = action.payload.data;
        }
      );
  },
});

export const selectMyBookings = (state: RootState) => state.bookings.myBookings;
export const selectBookingStatus = (state: RootState) => state.bookings.status;
// export const selectBookedTrips = (state: RootState) =>
//   state.bookings.bookedTrips;

export default bookingSlice.reducer;
