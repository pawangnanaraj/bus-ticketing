import React, { useEffect } from 'react';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import './myBookings.css';
import {
  selectMyBookings,
  myBookingsAsync,
  selectBookingStatus,
} from './myBookingSplice';
import { selectIsAuthenticated } from '../user/authSlice';
import { Navigate, useNavigate } from 'react-router-dom';
import Spinner from '../../utils/Spinner';
import {
  viewTicketAsync,
  selectViewTicketApproved,
} from '../viewTicket/viewTicketSplice';

export default function MyBookings() {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const myBookings = useAppSelector(selectMyBookings);
  const isAuthenticated = useAppSelector(selectIsAuthenticated);
  const status = useAppSelector(selectBookingStatus);
  const viewTicketApproved = useAppSelector(selectViewTicketApproved);

  useEffect(() => {
    dispatch(myBookingsAsync('any'));
  }, [dispatch]);

  const handleClick = (ticketId: string) => {
    dispatch(viewTicketAsync(ticketId));
    navigate('/viewTicket');
  };

  if (!isAuthenticated) {
    return <Navigate to='/login' />;
  }
  // console.log(viewTicketApproved);
  // if (viewTicketApproved) {
  //   return <Navigate to='/viewTicket' />;
  // }

  const myBookingsDetails = myBookings.map((value) => (
    <tr>
      <td>{value._id}</td>
      <td>{value.fromCity}</td>
      <td>{value.toCity}</td>
      <td>{value.travelStartDate}</td>
      <td>
        <button
          onClick={() => {
            handleClick(value._id);
          }}
        >
          View ticket
        </button>
      </td>
    </tr>
  ));

  const listMyBookings = () => {
    return (
      <table className='book-table'>
        <thead>
          <th>PNR</th>
          <th>From</th>
          <th>To</th>
          <th>Date of Journey</th>
          <th></th>
        </thead>
        <tbody>{myBookingsDetails}</tbody>
      </table>
    );
  };

  return (
    <div className='inner'>
      <h1>My Bookings</h1>
      {status === 'loading' ? <Spinner /> : listMyBookings()}
      <br></br>
    </div>
  );
}
