import { NextFunction, Response, Request } from 'express';
import Joi from 'joi';
export declare const validationMiddleware: (validator: Joi.ObjectSchema<any>) => (req: Request, res: Response, next: NextFunction) => void;
