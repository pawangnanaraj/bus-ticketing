"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validationMiddleware = void 0;
const validationMiddleware = (validator) => (req, res, next) => {
    let options = { abortEarly: false };
    const validation = validator.validate(req.body, options);
    if (!validation.error) {
        next();
    }
    else {
        const errors = [];
        for (let i = 0; i < validation.error.details.length; i++) {
            errors[i] = {
                msg: validation.error.details[i].message.replace(/['"]+/g, ''),
                param: validation.error.details[i].path[0],
            };
        }
        res.status(422).json({
            errors: errors,
        });
    }
};
exports.validationMiddleware = validationMiddleware;
