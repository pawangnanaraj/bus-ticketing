declare const httpStatusCodes: {
    OK: number;
    BAD_REQUEST: number;
    NOT_FOUND: number;
    INTERNAL_SERVER: number;
    UNAUTHORISED_USER: number;
};
export default httpStatusCodes;
