export declare class UserAlreadyAvailableError extends Error {
    message: string;
    status: number;
    constructor(message: string, status?: number);
}
export declare class NotFoundError extends Error {
    message: string;
    status: number;
    constructor(message: string, status?: number);
}
export declare class NotMatchedError extends Error {
    message: string;
    status: number;
    constructor(message: string, status?: number);
}
export declare class UnauthorizedUserError extends Error {
    message: string;
    status: number;
    constructor(message: string, status?: number);
}
