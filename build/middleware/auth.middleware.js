"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authMiddleware = void 0;
const jwt = __importStar(require("jsonwebtoken"));
const dotenv = __importStar(require("dotenv"));
dotenv.config();
let jwtKey = '';
process.env.JWT_KEY ? (jwtKey = process.env.JWT_KEY) : process.exit(1);
const authMiddleware = (req, res, next) => {
    //get token from header
    const token = req.header('x-auth-token');
    // console.log(`token: ${token}`);
    //check if no token
    if (token === undefined || null) {
        return res.sendStatus(401).json({ msg: 'No token, authotization denied' });
    }
    //if there is a token, verify the token
    try {
        // console.log('trying to decode');
        const decoded = jwt.verify(token, jwtKey);
        req.body.user = decoded;
        // console.info(decoded);
        next();
    }
    catch (err) {
        res.sendStatus(401).json({ msg: 'Token is not valid' });
    }
};
exports.authMiddleware = authMiddleware;
