"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const httpStatusCodes = {
    OK: 200,
    BAD_REQUEST: 400,
    NOT_FOUND: 404,
    INTERNAL_SERVER: 500,
    UNAUTHORISED_USER: 401,
};
exports.default = httpStatusCodes;
