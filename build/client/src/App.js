"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const Landing_1 = require("./features/landing/Landing");
const react_router_dom_1 = require("react-router-dom");
const hooks_1 = require("./app/hooks");
const setAuthToken_1 = __importDefault(require("./utils/setAuthToken"));
require("./App.css");
const Navbar_1 = __importDefault(require("./features/navbar/Navbar"));
const Register_1 = __importDefault(require("./features/user/Register"));
const Login_1 = __importDefault(require("./features/user/Login"));
const authSlice_1 = require("./features/user/authSlice");
function App() {
    const dispatch = (0, hooks_1.useAppDispatch)();
    (0, react_1.useEffect)(() => {
        // check for token in LS when app first runs
        if (localStorage.token) {
            // if there is a token set axios headers for all requests
            (0, setAuthToken_1.default)(localStorage.token);
        }
        // try to fetch a user, if no token or invalid token we
        // will get a 401 response from our API
        dispatch((0, authSlice_1.authAsync)(localStorage.token));
        // log user out from all tabs if they log out in one tab
        window.addEventListener('storage', () => {
            if (!localStorage.token) {
                dispatch((0, authSlice_1.logOut)());
            }
        });
        // eslint-disable-next-line
    }, []);
    return (<react_router_dom_1.BrowserRouter>
      <react_1.Fragment>
        <Navbar_1.default />
        <react_router_dom_1.Routes>
          <react_router_dom_1.Route path='/' element={<Landing_1.Landing />}/>
          <react_router_dom_1.Route path='/register' element={<Register_1.default />}/>
          <react_router_dom_1.Route path='/login' element={<Login_1.default />}/>
        </react_router_dom_1.Routes>
      </react_1.Fragment>
    </react_router_dom_1.BrowserRouter>);
}
exports.default = App;
