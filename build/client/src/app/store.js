"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.store = void 0;
const toolkit_1 = require("@reduxjs/toolkit");
const authSlice_1 = __importDefault(require("../features/user/authSlice"));
const alertSlice_1 = __importDefault(require("../features/alerts/alertSlice"));
exports.store = (0, toolkit_1.configureStore)({
    reducer: {
        alert: alertSlice_1.default,
        auth: authSlice_1.default,
    },
});
