import { TypedUseSelectorHook } from 'react-redux';
import type { RootState } from './store';
export declare const useAppDispatch: () => import("redux-thunk").ThunkDispatch<{
    alert: import("../features/alerts/alertSlice").AlertState;
    auth: import("../features/user/authSlice").AuthState;
}, null, import("redux").AnyAction> & import("redux-thunk").ThunkDispatch<{
    alert: import("../features/alerts/alertSlice").AlertState;
    auth: import("../features/user/authSlice").AuthState;
}, undefined, import("redux").AnyAction> & import("redux").Dispatch<import("redux").AnyAction>;
export declare const useAppSelector: TypedUseSelectorHook<RootState>;
