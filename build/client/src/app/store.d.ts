import { ThunkAction, Action } from '@reduxjs/toolkit';
export declare const store: import("@reduxjs/toolkit").EnhancedStore<{
    alert: import("../features/alerts/alertSlice").AlertState;
    auth: import("../features/user/authSlice").AuthState;
}, import("redux").AnyAction, [import("redux-thunk").ThunkMiddleware<{
    alert: import("../features/alerts/alertSlice").AlertState;
    auth: import("../features/user/authSlice").AuthState;
}, import("redux").AnyAction, null> | import("redux-thunk").ThunkMiddleware<{
    alert: import("../features/alerts/alertSlice").AlertState;
    auth: import("../features/user/authSlice").AuthState;
}, import("redux").AnyAction, undefined>]>;
export declare type AppDispatch = typeof store.dispatch;
export declare type RootState = ReturnType<typeof store.getState>;
export declare type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
