"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const api_utils_1 = __importDefault(require("./api.utils"));
const setAuthToken = (token) => {
    if (token) {
        api_utils_1.default.defaults.headers.common['x-auth-token'] = token;
        localStorage.setItem('token', token);
    }
    else {
        delete api_utils_1.default.defaults.headers.common['x-auth-token'];
        localStorage.removeItem('token');
    }
};
exports.default = setAuthToken;
