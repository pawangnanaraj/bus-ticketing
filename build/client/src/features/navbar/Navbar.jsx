"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const react_router_dom_1 = require("react-router-dom");
const navbar_module_css_1 = __importDefault(require("./navbar.module.css"));
const hooks_1 = require("../../app/hooks");
const authSlice_1 = require("../user/authSlice");
const authSlice_2 = require("../user/authSlice");
const NavBar = () => {
    const dispatch = (0, hooks_1.useAppDispatch)();
    const isAuthenticated = (0, hooks_1.useAppSelector)(authSlice_1.selectisAuthenticated);
    const onClick = (e) => {
        dispatch((0, authSlice_2.logOut)());
    };
    const guestLinks = (<ul className={navbar_module_css_1.default.navbar_ul}>
      <li>
        <react_router_dom_1.Link className={navbar_module_css_1.default.navbar_li} to='/register'>
          Sign Up
        </react_router_dom_1.Link>
      </li>
      <li>
        <react_router_dom_1.Link className={navbar_module_css_1.default.navbar_li} to='/login'>
          Login
        </react_router_dom_1.Link>
      </li>
    </ul>);
    const authLinks = (<ul className={navbar_module_css_1.default.navbar_ul}>
      <li>
        <react_router_dom_1.Link className={navbar_module_css_1.default.navbar_li} to='/myBookings'>
          <i className='fa fa-plane'/>{' '}
          <span className='hide-sm'>My Trips</span>
        </react_router_dom_1.Link>
      </li>
      <li>
        <react_router_dom_1.Link className={navbar_module_css_1.default.navbar_li} onClick={onClick} to='/'>
          <i className='fas fa-sign-out-alt'/>{' '}
          <span className='hide-sm'>Logout</span>
        </react_router_dom_1.Link>
      </li>
    </ul>);
    return (<nav className={navbar_module_css_1.default.navbar}>
      <h1>
        <react_router_dom_1.Link className={navbar_module_css_1.default.navbar_li} to='/'>
          <i className='fa fa-bus'/> Home
        </react_router_dom_1.Link>
      </h1>
      <react_1.Fragment>{isAuthenticated ? authLinks : guestLinks}</react_1.Fragment>
    </nav>);
};
exports.default = NavBar;
