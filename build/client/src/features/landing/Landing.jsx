"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Landing = void 0;
const react_1 = __importDefault(require("react"));
const Landing_module_css_1 = __importDefault(require("./Landing.module.css"));
function Landing() {
    return (<section className={Landing_module_css_1.default.dark_overlay}>
      <div className={Landing_module_css_1.default.landing_inner}>
        <h1 className={Landing_module_css_1.default.x_large}>Paw Bus</h1>
        <p className={Landing_module_css_1.default.lead}>Book your buses here to travel the world</p>
      </div>
    </section>);
}
exports.Landing = Landing;
