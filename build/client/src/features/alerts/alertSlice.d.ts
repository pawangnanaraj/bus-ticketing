import { Slice } from '@reduxjs/toolkit';
export interface AlertState {
    error: any;
    type: 'success' | 'error' | '';
}
export declare const alertSlice: Slice<AlertState>;
export declare const setAlerts: import("@reduxjs/toolkit").ActionCreatorWithoutPayload<string> | import("@reduxjs/toolkit").ActionCreatorWithPayload<any, string>;
declare const _default: import("redux").Reducer<AlertState, import("redux").AnyAction>;
export default _default;
