"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.setAlerts = exports.alertSlice = void 0;
const toolkit_1 = require("@reduxjs/toolkit");
const initialState = {
    error: '',
    type: '',
};
exports.alertSlice = (0, toolkit_1.createSlice)({
    name: 'alert',
    initialState,
    reducers: {
        setAlerts: (state, action) => {
            state.error = action.payload.errors;
            state.type = action.payload.type;
        },
    },
});
exports.setAlerts = exports.alertSlice.actions.setAlerts;
exports.default = exports.alertSlice.reducer;
