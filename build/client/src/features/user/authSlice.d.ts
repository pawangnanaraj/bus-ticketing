import { Slice } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
export declare const authAsync: import("@reduxjs/toolkit").AsyncThunk<unknown, string, {}>;
export declare const loginAsync: import("@reduxjs/toolkit").AsyncThunk<any, {
    email: string;
    password: string;
}, {}>;
export declare const registerAsync: import("@reduxjs/toolkit").AsyncThunk<any, {
    userName: string;
    email: string;
    password: string;
}, {}>;
export interface AuthState {
    isAuthenticated: true | false | null;
    status: 'idle' | 'loading' | 'failed';
    user: any;
    token: string | null;
    isError: Boolean;
    errorMessage: any;
}
export declare const authSlice: Slice<AuthState>;
export declare const selectisAuthenticated: (state: RootState) => boolean | null;
export declare const selectEmail: (state: RootState) => any;
export declare const selectPassword: (state: RootState) => any;
export declare const selectPassword2: (state: RootState) => any;
export declare const selectUserName: (state: RootState) => any;
export declare const logOut: import("@reduxjs/toolkit").ActionCreatorWithoutPayload<string> | import("@reduxjs/toolkit").ActionCreatorWithPayload<any, string>, setEmail: import("@reduxjs/toolkit").ActionCreatorWithoutPayload<string> | import("@reduxjs/toolkit").ActionCreatorWithPayload<any, string>, setPassword: import("@reduxjs/toolkit").ActionCreatorWithoutPayload<string> | import("@reduxjs/toolkit").ActionCreatorWithPayload<any, string>, setPassword2: import("@reduxjs/toolkit").ActionCreatorWithoutPayload<string> | import("@reduxjs/toolkit").ActionCreatorWithPayload<any, string>, setUserName: import("@reduxjs/toolkit").ActionCreatorWithoutPayload<string> | import("@reduxjs/toolkit").ActionCreatorWithPayload<any, string>;
declare const _default: import("redux").Reducer<AuthState, import("redux").AnyAction>;
export default _default;
