"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const hooks_1 = require("../../app/hooks");
const react_router_dom_1 = require("react-router-dom");
const react_fontawesome_1 = require("@fortawesome/react-fontawesome");
const free_solid_svg_icons_1 = require("@fortawesome/free-solid-svg-icons");
const user_module_css_1 = __importDefault(require("./user.module.css"));
const authSlice_1 = require("./authSlice");
const authSlice_2 = require("./authSlice");
const eye = <react_fontawesome_1.FontAwesomeIcon icon={free_solid_svg_icons_1.faEye}/>;
const RegisterUser = () => {
    const dispatch = (0, hooks_1.useAppDispatch)();
    const email = (0, hooks_1.useAppSelector)(authSlice_1.selectEmail);
    const password = (0, hooks_1.useAppSelector)(authSlice_1.selectPassword);
    const password2 = (0, hooks_1.useAppSelector)(authSlice_1.selectPassword2);
    const userName = (0, hooks_1.useAppSelector)(authSlice_1.selectUserName);
    const isAuthenticated = (0, hooks_1.useAppSelector)(authSlice_1.selectisAuthenticated);
    const onChangeEmail = (e) => {
        dispatch((0, authSlice_1.setEmail)(e.target.value));
    };
    const onChangePassword = (e) => {
        dispatch((0, authSlice_1.setPassword)(e.target.value));
    };
    const onChangePassword2 = (e) => {
        dispatch((0, authSlice_1.setPassword2)(e.target.value));
    };
    const onChangeUserName = (e) => {
        dispatch((0, authSlice_1.setUserName)(e.target.value));
    };
    const onSubmit = (e) => {
        e.preventDefault();
        const body = { userName, email, password };
        dispatch((0, authSlice_2.registerAsync)(body));
    };
    const [passwordShown, setPasswordShown] = (0, react_1.useState)(false);
    const togglePasswordVisibility = () => {
        setPasswordShown(passwordShown ? false : true);
    };
    if (isAuthenticated) {
        return <react_router_dom_1.Navigate to='/'/>;
    }
    return (<section className={user_module_css_1.default.container}>
      <h1 className={user_module_css_1.default.large}>Sign Up</h1>
      <p className={user_module_css_1.default.lead}>
        <i className='fas fa-user'/> Create Your Account
      </p>
      <form className={user_module_css_1.default.form} onSubmit={onSubmit}>
        <div className={user_module_css_1.default.form_group}>
          <input type='text' placeholder='Name' name='name' value={userName} onChange={onChangeUserName}/>
        </div>
        <div className={user_module_css_1.default.form_group}>
          <input className={user_module_css_1.default.form_textarea} type='email' placeholder='Email Address' name='email' value={email} onChange={onChangeEmail}/>
        </div>
        <div className={user_module_css_1.default.form_group}>
          <input type={passwordShown ? 'text' : 'password'} placeholder='Password' name='password' value={password} onChange={onChangePassword}/>
        </div>
        <div className={user_module_css_1.default.form_group}>
          <input type={passwordShown ? 'text' : 'password'} placeholder='Confirm Password' name='password2' value={password2} onChange={onChangePassword2}/>
          <i className={user_module_css_1.default.form_textarea_i} onClick={togglePasswordVisibility}>
            {eye}
          </i>
        </div>
        <input type='submit' className={user_module_css_1.default.btn_primary} value='Register'/>
      </form>
      <p className='my-1'>
        Already have an account? <react_router_dom_1.Link to='/login'>Sign In</react_router_dom_1.Link>
      </p>
    </section>);
};
exports.default = RegisterUser;
