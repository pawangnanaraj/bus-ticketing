"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const react_router_dom_1 = require("react-router-dom");
const hooks_1 = require("../../app/hooks");
const react_fontawesome_1 = require("@fortawesome/react-fontawesome");
const free_solid_svg_icons_1 = require("@fortawesome/free-solid-svg-icons");
const user_module_css_1 = __importDefault(require("./user.module.css"));
const authSlice_1 = require("./authSlice");
const authSlice_2 = require("./authSlice");
const eye = <react_fontawesome_1.FontAwesomeIcon icon={free_solid_svg_icons_1.faEye}/>;
const LoginUser = () => {
    const dispatch = (0, hooks_1.useAppDispatch)();
    const email = (0, hooks_1.useAppSelector)(authSlice_1.selectEmail);
    const password = (0, hooks_1.useAppSelector)(authSlice_1.selectPassword);
    const isAuthenticated = (0, hooks_1.useAppSelector)(authSlice_1.selectisAuthenticated);
    const onChangeEmail = (e) => {
        dispatch((0, authSlice_2.setEmail)(e.target.value));
    };
    const onChangePassword = (e) => {
        dispatch((0, authSlice_2.setPassword)(e.target.value));
    };
    const onSubmit = (e) => {
        e.preventDefault();
        const body = { email, password };
        dispatch((0, authSlice_1.loginAsync)(body));
    };
    const [passwordShown, setPasswordShown] = (0, react_1.useState)(false);
    const togglePasswordVisibility = () => {
        setPasswordShown(passwordShown ? false : true);
    };
    if (isAuthenticated) {
        return <react_router_dom_1.Navigate to='/'/>;
    }
    return (<section className={user_module_css_1.default.container}>
      <h1 className={user_module_css_1.default.large}>Log In</h1>
      <p className={user_module_css_1.default.lead}>
        <i className='fas fa-user'/> Sign in to book your ticket
      </p>
      <form className={user_module_css_1.default.form} onSubmit={onSubmit}>
        <div className={user_module_css_1.default.form_group}>
          <input className={user_module_css_1.default.form_textarea} type='email' placeholder='Email Address' name='email' value={email} onChange={onChangeEmail}/>
        </div>
        <div className={user_module_css_1.default.form_group}>
          <input type={passwordShown ? 'text' : 'password'} placeholder='Password' name='password' value={password} onInput={onChangePassword}/>
          <i className={user_module_css_1.default.form_textarea_i} onClick={togglePasswordVisibility}>
            {eye}
          </i>
        </div>

        <input type='submit' className={user_module_css_1.default.btn_primary} value='Sign In'/>
      </form>
      <p className='my-1'>
        Don't have an account? <react_router_dom_1.Link to='/register'>Register</react_router_dom_1.Link>
      </p>
    </section>);
};
exports.default = LoginUser;
