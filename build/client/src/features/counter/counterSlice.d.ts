import { Slice } from '@reduxjs/toolkit';
import { RootState, AppThunk } from '../../app/store';
export interface CounterState {
    value: number;
    status: 'idle' | 'loading' | 'failed';
}
export declare const incrementAsync: import("@reduxjs/toolkit").AsyncThunk<number, number, {}>;
export declare const counterSlice: Slice<CounterState>;
export declare const increment: import("@reduxjs/toolkit").ActionCreatorWithPayload<any, string> | import("@reduxjs/toolkit").ActionCreatorWithoutPayload<string>, decrement: import("@reduxjs/toolkit").ActionCreatorWithPayload<any, string> | import("@reduxjs/toolkit").ActionCreatorWithoutPayload<string>, incrementByAmount: import("@reduxjs/toolkit").ActionCreatorWithPayload<any, string> | import("@reduxjs/toolkit").ActionCreatorWithoutPayload<string>;
export declare const selectCount: (state: RootState) => number;
export declare const incrementIfOdd: (amount: number) => AppThunk;
declare const _default: import("redux").Reducer<CounterState, import("redux").AnyAction>;
export default _default;
