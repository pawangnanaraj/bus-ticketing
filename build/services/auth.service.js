"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_model_1 = require("../models/User.model");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const customErrors_1 = require("../middleware/customErrors");
const dotenv = __importStar(require("dotenv"));
dotenv.config();
let jwtKey = '';
process.env.JWT_KEY ? (jwtKey = process.env.JWT_KEY) : process.exit(1);
//----------------------------SignIn Service----------------------------------
const signIn = (email, password) => __awaiter(void 0, void 0, void 0, function* () {
    let user = yield User_model_1.UserModel.findOne({
        email: email,
    });
    if (!user) {
        throw new customErrors_1.NotMatchedError('Invalid Credentials');
    }
    //verify password
    const passwordMatch = yield bcrypt_1.default.compare(password, user.password);
    if (!passwordMatch) {
        throw new customErrors_1.NotMatchedError('Invalid Credentials');
    }
    //Generate the token
    const payload = {
        user: {
            id: user.id,
            isAgent: user.isAgent,
        },
    };
    const token = jsonwebtoken_1.default.sign(payload, jwtKey, { expiresIn: 36000 });
    return token;
});
//----------------------------Authenticate Service-----------------------------
const authenticate = (_id) => __awaiter(void 0, void 0, void 0, function* () {
    //get only id and leave the password
    const userId = yield User_model_1.UserModel.findById(_id).select('-password');
    if (userId == null) {
        // console.log('not found');
        throw new customErrors_1.NotFoundError('User not found');
    }
    // console.log(userId);
    return userId;
});
//----------------------------Export Auth Services-----------------------------
exports.default = {
    signIn,
    authenticate,
};
