/// <reference types="mongoose" />
declare const _default: {
    signIn: (email: string, password: string) => Promise<string>;
    authenticate: (_id: string) => Promise<import("mongoose").Document<any, any, import("../interfaces/user.interface").default> & import("../interfaces/user.interface").default & {
        _id: import("mongoose").Types.ObjectId;
    }>;
};
export default _default;
