declare const _default: {
    registerUser: (userName: string, email: string, password: string, isAgent: boolean) => Promise<string>;
};
export default _default;
