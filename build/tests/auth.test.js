"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const request = require('supertest');
const express_1 = __importDefault(require("express"));
const user_router_1 = require("../router/api/user.router");
const app = (0, express_1.default)();
app.use(express_1.default.json());
app.use('/api/register', user_router_1.registerUserRouter);
describe('Register User, Agent : /register', () => {
    jest.setTimeout(300000);
    //--------------------------Register User ------------------------------
    describe('Register User /', () => {
        it('Without Username', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app).post('/api/register').send({
                email: 'pawangr@gmail.com',
                password: '123456',
            });
            expect(res.statusCode).toEqual(422);
            expect(res.text).toContain('userName is required');
        }));
        it('Without email', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app).post('/api/register').send({
                userName: 'Pawan GR',
                password: '123456',
            });
            expect(res.statusCode).toEqual(422);
            expect(res.text).toContain('email is required');
        }));
        it('Without password', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app).post('/api/register').send({
                userName: 'Pawan GR',
                email: 'pawangr@gmail.com',
            });
            expect(res.statusCode).toEqual(422);
            expect(res.text).toContain('password is required');
        }));
        it('Successfully register user', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app)
                .post('/api/register')
                .set('Content-Type', 'application/json')
                .set('Accept', /json/)
                .send({
                userName: 'Pawan GR',
                email: 'pawangr@gmail.com',
                password: '123456',
            });
            expect(res.statusCode).toEqual(200);
            expect(res.text).toBeTruthy();
        }));
        it('With duplicate username', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app)
                .post('/api/register')
                .set('Accept', /json/)
                .set('Content-Type', 'application/json')
                .send({
                userName: 'Pawan GR',
                email: 'pawangr@gmail.com',
                password: '123456',
            });
            expect(res.text).toContain('user already exists');
            expect(res.statusCode).toEqual(400);
        }));
    });
    //--------------------------Register Agent ------------------------------
    describe('Register Agent /', () => {
        it('Without Username', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app).post('/api/register/agent').send({
                email: 'pawangr@gmail.com',
                password: '123456',
            });
            expect(res.statusCode).toEqual(422);
            expect(res.text).toContain('userName is required');
        }));
        it('Without email', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app).post('/api/register/agent').send({
                userName: 'Pawan GR',
                password: '123456',
            });
            expect(res.statusCode).toEqual(422);
            expect(res.text).toContain('email is required');
        }));
        it('Without password', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app).post('/api/register/agent').send({
                userName: 'Pawan GR',
                email: 'pawangr@gmail.com',
            });
            expect(res.statusCode).toEqual(422);
            expect(res.text).toContain('password is required');
        }));
        it('Successfully register user', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app)
                .post('/api/register/agent')
                .set('Content-Type', 'application/json')
                .set('Accept', /json/)
                .send({
                userName: 'Pawan GR',
                email: 'pawangr@gmail.com',
                password: '123456',
            });
            expect(res.statusCode).toEqual(200);
            expect(res.text).toBeTruthy();
        }));
        it('With duplicate username', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app)
                .post('/api/register/agent')
                .set('Accept', /json/)
                .set('Content-Type', 'application/json')
                .send({
                userName: 'Pawan GR',
                email: 'pawangr@gmail.com',
                password: '123456',
            });
            expect(res.text).toContain('user already exists');
            expect(res.statusCode).toEqual(400);
        }));
    });
});
