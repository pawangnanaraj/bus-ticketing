"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv = __importStar(require("dotenv"));
const connectMongoDB_1 = require("./helpers/connectMongoDB");
const user_router_1 = require("./router/api/user.router");
const path_1 = __importDefault(require("path"));
// import asyncWrapper from './helpers/asyncWrapper'
const cors_1 = __importDefault(require("cors"));
const auth_router_1 = require("./router/api/auth.router");
dotenv.config();
//initialise our app
const app = (0, express_1.default)();
(0, connectMongoDB_1.connectDB)();
app.use((0, cors_1.default)());
app.use(express_1.default.json());
app.use('/api/register', user_router_1.registerUserRouter);
app.use('/api/auth', auth_router_1.authRouter);
app.use(function (error, req, res, next) {
    res
        .status(error.status || error.statusCode || 500)
        .json({ errors: [{ msg: error.message || 'server Error' }] });
});
//Serve static assests in production
if (process.env.NODE_ENV === 'production') {
    //Set static folder
    app.use(express_1.default.static('client/build'));
    app.get('*', (req, res) => {
        res.sendFile(path_1.default.resolve('/app/client/build/index.html'));
    });
}
const PORT = process.env.DEV_PORT || 9000;
app.listen(PORT, () => console.log(`server running ${PORT} `));
exports.default = app;
