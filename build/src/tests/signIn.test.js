"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const request = require('supertest');
const express_1 = __importDefault(require("express"));
const auth_router_1 = require("../router/api/auth.router");
const user_router_1 = require("../router/api/user.router");
const app = (0, express_1.default)();
app.use(express_1.default.json());
app.use('/api/auth', auth_router_1.authRouter);
app.use('/api/register', user_router_1.registerUserRouter);
//initialising a user
const user = {
    userName: 'Pawan GR',
    email: 'pawangr@gmail.com',
    password: '123456',
};
//initialising a user
const agent = {
    userName: 'Jeswin GR',
    email: 'jeswingr@gmail.com',
    password: '123456',
};
let userRes;
let agentRes;
//-------------------------------Testing Login ----------------------------------
describe('Auth Router /api/auth', () => {
    jest.setTimeout(800000);
    beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
        userRes = yield request(app)
            .post('/api/register')
            .set('Content-Type', 'application/json')
            .set('Accept', /json/)
            .send(user);
        agentRes = yield request(app)
            .post('/api/register/agent')
            .set('Content-Type', 'application/json')
            .set('Accept', /json/)
            .send(agent);
    }));
    //-----------------------Testing User Login-------------------------------
    describe('Testing login', () => {
        it('Invalid password entry', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app)
                .post('/api/auth')
                .set('Content-Type', 'application/json')
                .set('Accept', /json/)
                .send({
                email: 'jeswingr@gmail.com',
                password: '12345',
            });
            expect(res.statusCode).toEqual(422);
            expect(res.text).toContain('password length must be at least 6 characters long');
        }));
        it('Invalid Email', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app)
                .post('/api/auth')
                .set('Content-Type', 'application/json')
                .set('Accept', /json/)
                .send({
                email: 'jeswing@gmail.com',
                password: '123456',
            });
            expect(res.statusCode).toEqual(401);
            expect(res.text).toContain('Invalid Credentials');
        }));
        it('Invalid password', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app)
                .post('/api/auth')
                .set('Content-Type', 'application/json')
                .set('Accept', /json/)
                .send({
                email: 'jeswingr@gmail.com',
                password: '123457',
            });
            expect(res.statusCode).toEqual(401);
            expect(res.text).toContain('Invalid Credentials');
        }));
        it('Successful login', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app)
                .post('/api/auth')
                .set('Content-Type', 'application/json')
                .set('Accept', /json/)
                .send({
                email: 'jeswingr@gmail.com',
                password: '123456',
            });
            expect(res.statusCode).toEqual(200);
            expect(res.text).toBeTruthy();
        }));
    });
    //---------------------------Testing user Authentication ---------------------------------
    describe('Testing getting user Auth token', () => {
        it('Without Token', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app).get('/api/auth');
            expect(res.statusCode).toEqual(401);
            expect(res.text).toContain('Unauthorized');
        }));
        it('Incorrect Token', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app)
                .get('/api/auth')
                .set('x-auth-token', 'asdfdf1322');
            expect(res.statusCode).toEqual(401);
            expect(res.text).toBe('Unauthorized');
        }));
        it('Correct User Token', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app)
                .get('/api/auth')
                .set('x-auth-token', userRes.body.token);
            expect(res.statusCode).toEqual(200);
            expect(res.body.userName).toBe(user.userName);
            expect(res.body.isAgent).toBe(false);
        }));
        it('Correct Agent Token', () => __awaiter(void 0, void 0, void 0, function* () {
            const res = yield request(app)
                .get('/api/auth')
                .set('x-auth-token', agentRes.body.token);
            expect(res.statusCode).toEqual(200);
            expect(res.body.userName).toBe(agent.userName);
            expect(res.body.isAgent).toBe(true);
        }));
    });
});
