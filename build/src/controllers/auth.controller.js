"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const auth_service_1 = __importDefault(require("../services/auth.service"));
let isAgent;
//----------------------------SignIn Controller------------------------------
const signIn = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    //see if the user exits, send an error
    const { email, password } = req.body;
    //   console.log(`email: ${email}, password: ${password}`);
    const token = yield auth_service_1.default.signIn(email, password);
    res.json({ token: token });
});
//----------------------------Authenticate Controller------------------------
const authenticate = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    //get only id and leave the password
    const id = req.body.user.user.id;
    // console.log(`_id: ${id}`);
    const userId = yield auth_service_1.default.authenticate(id);
    // console.log(`userId: ${userId}`);
    res.json(userId);
});
//----------------------------Export Auth Controller--------------------------
exports.default = {
    signIn,
    authenticate,
};
