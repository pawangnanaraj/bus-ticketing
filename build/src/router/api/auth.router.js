"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authRouter = void 0;
const express_1 = __importDefault(require("express"));
const auth_controller_1 = __importDefault(require("../../controllers/auth.controller"));
const asyncWrapper_1 = __importDefault(require("../../helpers/asyncWrapper"));
const validation_middleware_1 = require("../../middleware/validation.middleware");
const customValidation_1 = require("../../helpers/customValidation");
const auth_middleware_1 = require("../../middleware/auth.middleware");
exports.authRouter = express_1.default.Router();
// route --> POST  api/register
//authenticate user
exports.authRouter.get('/', auth_middleware_1.authMiddleware, (0, asyncWrapper_1.default)(auth_controller_1.default.authenticate));
// route --> POST  api/auth/
//login
exports.authRouter.post('/', (0, validation_middleware_1.validationMiddleware)(customValidation_1.loginValidation), (0, asyncWrapper_1.default)(auth_controller_1.default.signIn));
