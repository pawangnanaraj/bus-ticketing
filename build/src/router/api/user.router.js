"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.registerUserRouter = void 0;
const express_1 = __importDefault(require("express"));
const user_controller_1 = __importDefault(require("../../controllers/user.controller"));
const asyncWrapper_1 = __importDefault(require("../../helpers/asyncWrapper"));
const validation_middleware_1 = require("../../middleware/validation.middleware");
const customValidation_1 = require("../../helpers/customValidation");
exports.registerUserRouter = express_1.default.Router();
// route --> POST  api/register
//register user
exports.registerUserRouter.post('/', (0, validation_middleware_1.validationMiddleware)(customValidation_1.registerValidation), (0, asyncWrapper_1.default)(user_controller_1.default.registerUser));
// route --> POST  api/register/agent
//register agent
exports.registerUserRouter.post('/agent', (0, validation_middleware_1.validationMiddleware)(customValidation_1.registerValidation), (0, asyncWrapper_1.default)(user_controller_1.default.registerUser));
