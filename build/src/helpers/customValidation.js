"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.loginValidation = exports.registerValidation = void 0;
const joi_1 = __importDefault(require("joi"));
exports.registerValidation = joi_1.default.object().keys({
    userName: joi_1.default.string().lowercase().trim().min(3).required(),
    email: joi_1.default.string().lowercase().trim().email().required(),
    password: joi_1.default.string().alphanum().min(6).max(8).required(),
});
exports.loginValidation = joi_1.default.object().keys({
    email: joi_1.default.string().lowercase().trim().email().required(),
    password: joi_1.default.string().alphanum().min(6).max(8).required(),
});
