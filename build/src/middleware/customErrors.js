"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnauthorizedUserError = exports.NotMatchedError = exports.NotFoundError = exports.UserAlreadyAvailableError = void 0;
const errorCodes_middleware_1 = __importDefault(require("./errorCodes.middleware"));
class UserAlreadyAvailableError extends Error {
    constructor(message, status = errorCodes_middleware_1.default.BAD_REQUEST) {
        super(message);
        this.message = message;
        this.status = status;
        this.name = 'UserAlreadyAvailableError';
    }
}
exports.UserAlreadyAvailableError = UserAlreadyAvailableError;
class NotFoundError extends Error {
    constructor(message, status = errorCodes_middleware_1.default.NOT_FOUND) {
        super(message);
        this.message = message;
        this.status = status;
        this.name = 'notFoundError';
    }
}
exports.NotFoundError = NotFoundError;
class NotMatchedError extends Error {
    constructor(message, status = errorCodes_middleware_1.default.UNAUTHORISED_USER) {
        super(message);
        this.message = message;
        this.status = status;
        this.name = 'NotMatchError';
    }
}
exports.NotMatchedError = NotMatchedError;
class UnauthorizedUserError extends Error {
    constructor(message, status = errorCodes_middleware_1.default.UNAUTHORISED_USER) {
        super(message);
        this.message = message;
        this.status = status;
        this.name = 'unauthorizedUserError';
    }
}
exports.UnauthorizedUserError = UnauthorizedUserError;
