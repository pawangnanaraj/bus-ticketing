import { Request, Response, NextFunction } from 'express';
export default function promiseWrapper(fn: any): (req: Request, res: Response, next: NextFunction) => void;
