"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function promiseWrapper(fn) {
    return (req, res, next) => {
        fn(req, res).catch(next);
    };
}
exports.default = promiseWrapper;
