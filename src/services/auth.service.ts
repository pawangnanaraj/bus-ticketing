import { UserModel } from '../models/User.model';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { NotFoundError, NotMatchedError } from '../middleware/customErrors';
import * as dotenv from 'dotenv';

dotenv.config();

let jwtKey = '';

process.env.JWT_KEY ? (jwtKey = process.env.JWT_KEY) : process.exit(1);

//----------------------------SignIn Service----------------------------------
const signIn = async (email: string, password: string) => {
  let user = await UserModel.findOne({
    email: email,
  });
  if (!user) {
    throw new NotMatchedError('Invalid Credentials');
  }
  //verify password
  const passwordMatch = await bcrypt.compare(password, user.password);

  if (!passwordMatch) {
    throw new NotMatchedError('Invalid Credentials');
  }

  //Generate the token
  const payload = {
    user: {
      id: user.id,
      isAgent: user.isAgent,
    },
  };

  const token = jwt.sign(payload, jwtKey, { expiresIn: 36000 });

  return token;
};

//----------------------------Authenticate Service-----------------------------
const authenticate = async (_id: string) => {
  //get only id and leave the password
  const userId = await UserModel.findById(_id).select('-password');

  if (userId == null) {
    // console.log('not found');
    // throw new NotFoundError('User not found');
  }
  // console.log(userId);
  return userId;
};

//----------------------------Export Auth Services-----------------------------
export default {
  signIn,
  authenticate,
};
