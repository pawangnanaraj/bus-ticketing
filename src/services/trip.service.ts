import { NotFoundError } from '../middleware/customErrors';
import { TripModel } from '../models/Trip.model';

//--------------------------Get all from City service -------------------------
const getFromCity = async () => {
  const fromCity = await TripModel.find({}).select('fromCity -_id');

  const fromCities = [...new Set(fromCity.map((x) => x.fromCity))];

  return fromCities;
};

//--------------------------Get all from City service -------------------------
const getToCity = async (fromCity: string[]) => {
  const toCity = await TripModel.find({ fromCity: fromCity }).select('toCity');

  const toCities = [...new Set(toCity.map((x) => x.toCity))];

  return toCities;
};

//--------------------------- Search trips service-----------------------------
//user should be able to search
const searchTrips = async (
  fromCity: string,
  toCity: string,
  travelDate: string
) => {
  console.log(fromCity, toCity, travelDate);
  const trips = await TripModel.find({
    fromCity: fromCity,
    toCity: toCity,
    travelStartDate: travelDate,
  });

  if (trips.length < 1) {
    throw new NotFoundError('No Buses in this Route');
  }

  return trips;
};

//--------------------------- Search trips service-----------------------------
const viewTripById = async (tripId: string) => {
  const trip = await TripModel.findById(tripId);

  if (!trip) {
    throw new NotFoundError('Trip Not Found');
  }

  return trip;
};

export default {
  searchTrips,
  getFromCity,
  getToCity,
  viewTripById,
};
