import { ObjectId } from 'mongoose';
import Bus from '../interfaces/bus.interface';
import Seat from '../interfaces/seat.interface';
import { BusModel } from '../models/Bus.model';
import { UserModel } from '../models/User.model';
import { TripModel } from '../models/Trip.model';
import {
  NotFoundError,
  UnauthorizedUserError,
  BusAlreadyExistsError,
  SeatsAlreadyExistsError,
} from '../middleware/customErrors';
import { getDay, parseISO, nextDay, format } from 'date-fns';
import Trip from '../interfaces/trip.interface';
import { SeatModel } from '../models/Seat.model';

//------------ Create Bus Service ----------
const createBus = async (busDetails: Bus, userId: ObjectId) => {
  //get the agentname and id and check if the user is agent
  const userDetails = await UserModel.findById(userId).select(
    'userName email isAgent'
  );
  if (!userDetails) {
    throw new NotFoundError('user not Found');
  }
  if (!userDetails.isAgent) {
    throw new UnauthorizedUserError('You are not authorised to create a bus');
  }
  busDetails = {
    ...busDetails,
    agentId: userDetails._id,
  };

  const busExist = await BusModel.find(busDetails);

  if (busExist.length > 0) {
    throw new BusAlreadyExistsError('You have already created this bus');
  }

  const bus = new BusModel(busDetails);
  const newbus = await bus.save();

  await createTrip(newbus);
  await createSeat(newbus._id);

  return newbus;
};

//---------------------------- Create Trips ---------------------------
const createTrip = async (busDetails: Bus) => {
  let {
    busName,
    busType,
    _id,
    agentId,
    fromCity,
    toCity,
    totalSeats,
    pricePerSeat,
    firstStartDate,
    travelStartTime,
    firstReachingDate,
    frequency,
    reachingTime,
    validTillDate,
    ...rest
  } = busDetails;

  //-------------------- Creating Trip Dates -----------------------
  let startingTime = Date.parse(firstStartDate + 'T' + travelStartTime);
  let reachTime = Date.parse(firstReachingDate + 'T' + reachingTime);
  let travelTime = reachTime - startingTime;
  const numberOfTripsPerWeek = frequency.length;

  let validDateTill: Date = parseISO(validTillDate);
  let firstDate: Date = parseISO(firstStartDate);

  let tripDateString: String,
    day: any,
    indexInFrequency: any,
    nextTripDay: any,
    tripReachingDate: any;
  let tripDates: any = [];
  let reachingDates: any = [];

  for (let tripDate = firstDate; tripDate <= validDateTill; ) {
    tripDateString = format(tripDate, 'yyyy-MM-dd');
    tripReachingDate = format(
      Date.parse(tripDateString + 'T' + travelStartTime) + travelTime,
      'yyyy-MM-dd'
    );
    tripDates.push(tripDateString);
    reachingDates.push(tripReachingDate);
    //find the day of the start date or the tripdate
    day = getDay(tripDate);
    //find the index of it in the given array
    indexInFrequency = frequency.indexOf(day);
    //if the index is 6 and the total trips is 7 reset the index
    if (indexInFrequency === numberOfTripsPerWeek - 1) {
      nextTripDay = 0;
    } else {
      //increment to find the next day in the freq array
      nextTripDay = indexInFrequency + 1;
    }
    //create the next trip date
    tripDate = nextDay(tripDate, frequency[nextTripDay]);
  }

  const totalTrips = tripDates.length;
  let tripList: any = [];

  for (let i = 0; i < totalTrips; i++) {
    let ithTrip = {} as Trip;
    ithTrip = {
      ...ithTrip,
      busName,
      agentId,
      busType,
      fromCity,
      toCity,
      totalSeats,
      availableSeats: totalSeats,
      pricePerSeat,
      travelStartTime,
      reachingTime,
      busId: _id ? _id : '',
      cancelled: false,
    };
    ithTrip.reachingDate = reachingDates[i];
    ithTrip.travelStartDate = tripDates[i];
    tripList.push(ithTrip);
  }

  await TripModel.insertMany(tripList);
};

//---------------------------- Create Seats ---------------------------
const createSeat = async (busId: any) => {
  const bus = await SeatModel.find({ busId: busId });

  if (bus.length > 0) {
    throw new SeatsAlreadyExistsError(
      'You have already created seats for this bus'
    );
  }

  const trips = await TripModel.find({ busId: busId }).select(
    'busId _id totalSeats'
  );
  if (trips.length < 1) {
    throw new NotFoundError('Trip not found');
  }

  // console.log('trips details: ', trips);
  const totalTrips = trips.length;

  console.log('totalTrips : ', totalTrips);
  let tripSeats = [];

  for (let i = 0; i < totalTrips; i++) {
    let totalTripSeats = 0;

    totalTripSeats = trips[i].totalSeats;

    console.log('totalTripSeats', totalTripSeats, i, trips[i]);
    for (let s = 1; s <= totalTripSeats; s++) {
      let seat = {} as Seat;
      console.log(i, s);

      seat = {
        ...seat,
        seatNumber: s,
        tripId: trips[i]._id,
        busId: trips[i].busId,
      };
      tripSeats.push(seat);
    }
  }
  await SeatModel.insertMany(tripSeats);
};

export default {
  createBus,
  createTrip,
  createSeat,
};
