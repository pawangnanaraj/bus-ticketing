import Seat from '../interfaces/seat.interface';
import { DuplicateTicketError } from '../middleware/customErrors';
import Ticket from '../interfaces/Ticket.interface';
import {
  NotFoundError,
  SeatNotAvailableError,
  InvalidSeatRequestError,
} from '../middleware/customErrors';
import { SeatModel } from '../models/Seat.model';
import { TicketModel } from '../models/Ticket.model';
import { TripModel } from '../models/Trip.model';

//------------------------ View all seats of a trip-------------------------
const viewSeats = async (tripId: string) => {
  const seats = await SeatModel.find({ tripId: tripId });

  if (seats.length < 1) {
    throw new NotFoundError('Seats not found for this trips');
  }
  return seats;
};

//------------------------ Get details of seats ---------------------
const seatDetails = async (seatId: string[]) => {
  const seatDetail = await SeatModel.find({
    _id: seatId,
  }).select('status');
  if (seatDetail.length < 1) {
    throw new NotFoundError('Seat Details not found');
  }
  return seatDetail;
};

//--------------------- get all available Seats in a trip --------------------
const availableSeats = async (tripId: Object) => {
  const availableSeats = await SeatModel.find({
    tripId: tripId,
    status: 'available',
  }).select('seatNumber');

  if (availableSeats.length < 1) {
    throw new NotFoundError('No Seat available');
  }

  //update the details in the trip
  await TripModel.findByIdAndUpdate(
    {
      _id: tripId,
    },
    { $set: { availableSeats: availableSeats.length } }
  );

  return availableSeats;
};

//--------------------- update the status of seats ------------------------------

// const updateSeatStatus = async (
//   seatId: string[],
//   userId: string,
//   status: string
// ) => {
//   let selectedSeat;
//   let updatedSeats = [];
//   for (let i = 0; i < seatId.length; i++) {
//     const seat = await SeatModel.findById(seatId[i]);
//     if (!seat) {
//       throw new NotFoundError('Seat not found');
//     }
//     if (seat.status === 'booked') {
//       throw new SeatNotAvailableError('Please seat another seat');
//     }
//     if (seat.status === status) {
//       throw new InvalidSeatRequestError(
//         'The seat is already blocked by another user please select another seat'
//       );
//     }
//     selectedSeat = await SeatModel.findByIdAndUpdate(
//       {
//         _id: seat?._id,
//       },
//       { $set: { status: status, userId: userId } },
//       { new: true }
//     );
//     updatedSeats.push(selectedSeat);

//     //------------------reset the seat after 1 mins if not booked---------------
//     setTimeout(async () => {
//       const resetSeat = await SeatModel.findById(seatId[i]);

//       if (resetSeat?.status !== 'booked') {
//         await SeatModel.findByIdAndUpdate(
//           {
//             _id: resetSeat?._id,
//           },
//           { $set: { status: 'available', userId: '' } }
//         );
//         console.log('seatId: ', resetSeat?._id, 'reset to available');
//       } else {
//         console.log('seatId: ', resetSeat?._id, 'is booked');
//       }
//     }, 60000);
//   }

//   return updatedSeats;
// };

//--------------------- update the status of seats ------------------------------

const NewUpdateSeatStatus = async (
  seatId: string[],
  userId: string,
  status: string
) => {
  let selectedSeat;
  let updatedSeats = [];
  if (status === 'booked') {
    for (let i = 0; i < seatId.length; i++) {
      selectedSeat = await SeatModel.findOneAndUpdate(
        {
          $and: [
            { _id: seatId[i] },
            { status: { $ne: 'booked' } },
            { userId: userId },
          ],
        },
        { $set: { status: status, userId: userId } },
        { new: true }
      ).orFail(
        new InvalidSeatRequestError('The selected seat cannot be booked')
      );
      updatedSeats.push(selectedSeat);
    }
  } else if (status === 'hold') {
    for (let i = 0; i < seatId.length; i++) {
      selectedSeat = await SeatModel.findOneAndUpdate(
        {
          $and: [{ _id: seatId[i] }, { status: 'available' }],
        },
        { $set: { status: status, userId: userId } },
        { new: true }
      ).orFail(
        new InvalidSeatRequestError(
          'The selected seat cannot be blocked. Please select another seat'
        )
      );
      updatedSeats.push(selectedSeat);

      //------------------Have to explore how to implement using monogoDB TTL ------
      //------------------reset the seat after 1 mins if not booked---------------
      setTimeout(async () => {
        const resetSeat = await SeatModel.findById(seatId[i]);

        if (resetSeat?.status !== 'booked') {
          await SeatModel.findByIdAndUpdate(
            {
              _id: resetSeat?._id,
            },
            { $set: { status: 'available', userId: '' } }
          );
          console.log('seatId: ', resetSeat?._id, 'reset to available');
        } else {
          console.log('seatId: ', resetSeat?._id, 'is booked');
        }
      }, 60000);
    }
  }

  return updatedSeats;
};

// ------------------- Book a ticket --------------------------------
const bookATicket = async (ticketDetails: Ticket) => {
  const passengerNames = [];
  for (let i = 0; i < ticketDetails.passengerDetails.length; i++) {
    passengerNames.push(ticketDetails.passengerDetails[i].passengerName);
  }

  const duplicateTicket = await TicketModel.find({
    cancelled: false,
    userId: ticketDetails.userId,
    tripId: ticketDetails.tripId,
    emailId: ticketDetails.emailId,
    passengerDetails: {
      $elemMatch: { passengerName: { $in: passengerNames } },
    },
  });
  //----------------check if it is a duplicate ticket ---------------------
  if (duplicateTicket.length > 0) {
    throw new DuplicateTicketError(
      'You have already booked this ticket, please check your bookings'
    );
  }

  const ticket = new TicketModel(ticketDetails);
  const newTicket = await ticket.save();

  //---------------- update ticket status in the seats -------------------
  newTicket.passengerDetails.map((value) => {
    NewUpdateSeatStatus([value.seatId], ticketDetails.userId, 'booked');
  });

  // await availableSeats(ticketDetails.tripId);
  return 'newTicket';
};

//--------------------view a ticket by Id ---------------------------
const viewTicketById = async (ticketId: string) => {
  const ticket = await TicketModel.findById(ticketId);

  if (!ticket) {
    throw new NotFoundError('Ticket not Found');
  }

  return ticket;
};
//--------------------   view my bookings ---------------------------
const myBookings = async (userId: string) => {
  const allBookings = await TicketModel.find({ userId: userId });

  return allBookings;
};

export default {
  viewSeats,
  seatDetails,
  availableSeats,
  bookATicket,
  NewUpdateSeatStatus,
  myBookings,
  viewTicketById,
};
