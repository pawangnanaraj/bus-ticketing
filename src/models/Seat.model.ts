import { Schema, model } from 'mongoose';
import Seat from '../interfaces/seat.interface';

const SeatSchema = new Schema<Seat>({
  status: {
    type: String,
    default: 'available',
  },
  busId: {
    type: String,
    required: true,
  },
  tripId: {
    type: String,
    required: true,
  },
  seatNumber: {
    type: Number,
    required: true,
  },
  userId: {
    type: String,
    default: '',
  },
});

export const SeatModel = model<Seat>('Seat', SeatSchema);
