import { Schema, model } from 'mongoose';
import User from '../interfaces/user.interface';

const UserSchema = new Schema<User>(
  {
    userName: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    isAgent: { type: Boolean, default: false },
  },
  { timestamps: true }
);

export const UserModel = model<User>('User', UserSchema);
