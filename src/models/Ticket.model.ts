import { Schema, model } from 'mongoose';
import Ticket from '../interfaces/Ticket.interface';

const TicketSchema = new Schema<Ticket>({
  cancelled: {
    type: Boolean,
    default: false,
  },
  userId: {
    type: String,
    ref: 'Users',
    required: true,
  },
  tripId: {
    type: String,
    required: true,
    ref: 'Trips',
  },
  busName: {
    type: String,
    required: true,
  },
  fromCity: {
    type: String,
    required: true,
  },
  toCity: {
    type: String,
    required: true,
  },
  travelStartDate: {
    type: String,
    required: true,
  },
  travelStartTime: {
    type: String,
    required: true,
  },
  reachingDate: {
    type: String,
    required: true,
  },
  reachingTime: {
    type: String,
    required: true,
  },
  passengerDetails: [
    {
      passengerName: {
        type: String,
        required: true,
      },
      seatNumber: {
        type: Number,
        required: true,
      },
      seatId: {
        type: String,
        required: true,
        ref: 'Seat',
      },
      gender: {
        type: String,
        required: true,
      },
    },
  ],
  totalPrice: {
    type: Number,
    required: true,
  },
  emailId: {
    type: String,
    required: true,
  },
});

export const TicketModel = model<Ticket>('Ticket', TicketSchema);
