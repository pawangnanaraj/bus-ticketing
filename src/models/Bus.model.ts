import { Schema, model } from 'mongoose';
import Bus from '../interfaces/bus.interface';
import addDays from 'date-fns/addDays';
import { string } from 'joi';

const BusSchema = new Schema<Bus>(
  {
    busName: {
      type: String,
      required: true,
    },
    agentId: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    busType: {
      type: String,
      required: true,
    },
    fromCity: {
      type: String,
      required: true,
    },
    toCity: {
      type: String,
      required: true,
    },
    totalSeats: {
      type: Number,
      required: true,
      default: 18,
    },
    pricePerSeat: {
      type: Number,
      required: true,
    },
    firstStartDate: {
      type: String,
      required: true,
    },
    travelStartTime: {
      type: String,
      required: true,
    },
    firstReachingDate: {
      type: String,
      required: true,
    },
    reachingTime: {
      type: String,
      required: true,
    },
    frequency: [
      {
        type: Number,
        required: true,
      },
    ],
    validFromDate: {
      type: String,
    },
    validTillDate: {
      type: String,
    },
    seatLayout: {
      type: [Number],
      required: true,
      default: [1, 2],
    },
  },
  { timestamps: true }
);

export const BusModel = model<Bus>('Bus', BusSchema);
