import { Schema, model } from 'mongoose';
import Trip from '../interfaces/trip.interface';

const TripSchema = new Schema<Trip>(
  {
    cancelled: {
      type: Boolean,
      default: false,
    },
    busName: {
      type: String,
      required: true,
    },
    busId: {
      type: String,
      ref: 'Bus',
    },
    agentId: {
      type: String,
      ref: 'User',
    },
    busType: {
      type: String,
      required: true,
    },
    fromCity: {
      type: String,
      required: true,
    },
    toCity: {
      type: String,
      required: true,
    },
    totalSeats: {
      type: Number,
      required: true,
      default: 18,
    },
    availableSeats: {
      type: Number,
    },
    pricePerSeat: {
      type: Number,
      required: true,
    },
    travelStartDate: {
      type: String,
      required: true,
    },
    travelStartTime: {
      type: String,
      required: true,
    },
    reachingDate: {
      type: String,
      required: true,
    },
    reachingTime: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

export const TripModel = model<Trip>('Trip', TripSchema);
