import httpStatusCodes from './errorCodes.middleware';

export class UserAlreadyAvailableError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.BAD_REQUEST
  ) {
    super(message);
    this.name = 'UserAlreadyAvailableError';
  }
}

export class NotFoundError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.NOT_FOUND
  ) {
    super(message);
    this.name = 'notFoundError';
  }
}

export class NotMatchedError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.UNAUTHORISED_USER
  ) {
    super(message);
    this.name = 'NotMatchError';
  }
}

export class UnauthorizedUserError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.UNAUTHORISED_USER
  ) {
    super(message);
    this.name = 'unauthorizedUserError';
  }
}

export class InvalidDateError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.BAD_REQUEST
  ) {
    super(message);
    this.name = 'invalidDateError';
  }
}

export class BusAlreadyExistsError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.BAD_REQUEST
  ) {
    super(message);
    this.name = 'busAlreadyExistsError';
  }
}

export class InvalidSeatsError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.BAD_REQUEST
  ) {
    super(message);
    this.name = 'invalidSeatsError';
  }
}

export class SeatsAlreadyExistsError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.BAD_REQUEST
  ) {
    super(message);
    this.name = 'seatsAlreadyExistsError';
  }
}

export class InvalidQueryError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.BAD_REQUEST
  ) {
    super(message);
    this.name = 'InvalidQueryError';
  }
}
export class SeatNotAvailableError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.UNAUTHORISED_USER
  ) {
    super(message);
    this.name = 'SeatNotAvailableError';
  }
}

export class InvalidSeatRequestError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.BAD_REQUEST
  ) {
    super(message);
    this.name = 'InvalidSeatRequestError';
  }
}

export class DuplicateTicketError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.BAD_REQUEST
  ) {
    super(message);
    this.name = 'DuplicateTicketError';
  }
}
