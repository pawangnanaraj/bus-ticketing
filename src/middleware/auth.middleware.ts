import * as jwt from 'jsonwebtoken';
import { Response, Request, NextFunction } from 'express';
import * as dotenv from 'dotenv';

dotenv.config();

let jwtKey = '';

process.env.JWT_KEY ? (jwtKey = process.env.JWT_KEY) : process.exit(1);

export const authMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  //get token from header
  const token = req.header('x-auth-token');
  // console.log(`token: ${token}`);
  //check if no token
  if (token === undefined || null) {
    return res.sendStatus(401).json({ msg: 'No token, authotization denied' });
  }

  //if there is a token, verify the token
  try {
    // console.log('trying to decode');
    const decoded = jwt.verify(token, jwtKey);
    req.body.user = decoded;
    // console.info(decoded);
    next();
  } catch (err: any) {
    res.sendStatus(401).json({ msg: 'Token is not valid' });
  }
};
