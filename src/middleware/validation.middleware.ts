import { NextFunction, Response, Request } from 'express';
import Joi from 'joi';

export const validationMiddleware =
  (validator: Joi.ObjectSchema<any>) =>
  (req: Request, res: Response, next: NextFunction) => {
    let options = { abortEarly: false };
    const validation = validator.validate(req.body || req.query, options);

    if (!validation.error) {
      next();
    } else {
      const errors = [];
      for (let i = 0; i < validation.error.details.length; i++) {
        errors[i] = validation.error.details[i].message.replace(/['"]+/g, '');
        // errors[i] = {
        //   msg: validation.error.details[i].message.replace(/['"]+/g, ''),
        //   param: validation.error.details[i].path[0],
        // };
      }
      res.status(422).json({
        errors: errors,
      });
    }
  };
