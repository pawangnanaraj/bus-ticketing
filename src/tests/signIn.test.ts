const request = require('supertest');
import express, { Application } from 'express';
import { authRouter } from '../router/api/auth.router';
import { registerUserRouter } from '../router/api/user.router';

const app: Application = express();

app.use(express.json());
app.use('/api/auth', authRouter);
app.use('/api/register', registerUserRouter);

//initialising a user
const user = {
  userName: 'Pawan GR',
  email: 'pawangr@gmail.com',
  password: '123456',
};

//initialising a user
const agent = {
  userName: 'Jeswin GR',
  email: 'jeswingr@gmail.com',
  password: '123456',
};

let userRes: any;
let agentRes: any;
//-------------------------------Testing Login ----------------------------------
describe('Auth Router /api/auth', () => {
  jest.setTimeout(800000);
  beforeAll(async () => {
    userRes = await request(app)
      .post('/api/register')
      .set('Content-Type', 'application/json')
      .set('Accept', /json/)
      .send(user);
    agentRes = await request(app)
      .post('/api/register/agent')
      .set('Content-Type', 'application/json')
      .set('Accept', /json/)
      .send(agent);
  });

  //-----------------------Testing User Login-------------------------------
  describe('Testing login', () => {
    it('Invalid password entry', async () => {
      const res = await request(app)
        .post('/api/auth')
        .set('Content-Type', 'application/json')
        .set('Accept', /json/)
        .send({
          email: 'jeswingr@gmail.com',
          password: '12345',
        });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain(
        'password length must be at least 6 characters long'
      );
    });

    it('Invalid Email', async () => {
      const res = await request(app)
        .post('/api/auth')
        .set('Content-Type', 'application/json')
        .set('Accept', /json/)
        .send({
          email: 'jeswing@gmail.com',
          password: '123456',
        });
      expect(res.statusCode).toEqual(401);
      expect(res.text).toContain('Invalid Credentials');
    });

    it('Invalid password', async () => {
      const res = await request(app)
        .post('/api/auth')
        .set('Content-Type', 'application/json')
        .set('Accept', /json/)
        .send({
          email: 'jeswingr@gmail.com',
          password: '123457',
        });
      expect(res.statusCode).toEqual(401);
      expect(res.text).toContain('Invalid Credentials');
    });

    it('Successful login', async () => {
      const res = await request(app)
        .post('/api/auth')
        .set('Content-Type', 'application/json')
        .set('Accept', /json/)
        .send({
          email: 'jeswingr@gmail.com',
          password: '123456',
        });
      expect(res.statusCode).toEqual(200);
      expect(res.text).toBeTruthy();
    });
  });

  //---------------------------Testing user Authentication ---------------------------------
  describe('Testing getting user Auth token', () => {
    it('Without Token', async () => {
      const res = await request(app).get('/api/auth');
      expect(res.statusCode).toEqual(401);
      expect(res.text).toContain('Unauthorized');
    });

    it('Incorrect Token', async () => {
      const res = await request(app)
        .get('/api/auth')
        .set('x-auth-token', 'asdfdf1322');
      expect(res.statusCode).toEqual(401);
      expect(res.text).toBe('Unauthorized');
    });

    it('Correct User Token', async () => {
      const res = await request(app)
        .get('/api/auth')
        .set('x-auth-token', userRes.body.token);
      expect(res.statusCode).toEqual(200);
      expect(res.body.userName).toBe(user.userName);
      expect(res.body.isAgent).toBe(false);
    });

    it('Correct Agent Token', async () => {
      const res = await request(app)
        .get('/api/auth')
        .set('x-auth-token', agentRes.body.token);
      expect(res.statusCode).toEqual(200);
      expect(res.body.userName).toBe(agent.userName);
      expect(res.body.isAgent).toBe(true);
    });
  });
});
