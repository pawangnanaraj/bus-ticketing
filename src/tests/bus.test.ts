const request = require('supertest');
import express, { Application } from 'express';
import { busRouter } from '../router/api/bus.router';
import { registerUserRouter } from '../router/api/user.router';
import mongoose from 'mongoose';
import { mongo } from './setup';

const app: Application = express();

app.use(express.json());
app.use('/api/bus', busRouter);
app.use('/api/register', registerUserRouter);

//initialising a user
const user = {
  userName: 'Pawan GR',
  email: 'pawangr@gmail.com',
  password: '123456',
};

//initialising a user
const agent = {
  userName: 'Jeswin GR',
  email: 'jeswingr@gmail.com',
  password: '123456',
};

let userRes: any;
let agentRes: any;

describe('Create Bus, Agent : /bus/create', () => {
  jest.setTimeout(500000);
  beforeAll(async () => {
    userRes = await request(app)
      .post('/api/register')
      .set('Content-Type', 'application/json')
      .set('Accept', /json/)
      .send(user);
    agentRes = await request(app)
      .post('/api/register/agent')
      .set('Content-Type', 'application/json')
      .set('Accept', /json/)
      .send(agent);
  });

  //-------------------------- Create Bus ------------------------------
  describe('Create Bus /create', () => {
    it('User tries to create a bus', async () => {
      const res = await request(app)
        .post('/api/bus/create')
        .set('Accept', /json/)
        .set('Content-Type', 'application/json')
        .set('x-auth-token', userRes.body.token)
        .send({
          busName: 'Tipu Sultan Travels',
          busType: 'Sleeper',
          fromCity: 'Bangalore',
          toCity: 'Chennai',
          totalSeats: 21,
          pricePerSeat: 700,
          firstStartDate: '2031-11-29',
          travelStartTime: '21:30',
          firstReachingDate: '2031-11-30',
          reachingTime: '06:00',
          frequency: [0, 1, 2, 3, 4, 5, 6],
          seatLayout: [1, 2],
        });
      expect(res.statusCode).toEqual(401);
      expect(res.text).toContain('You are not authorised to create a bus');
    });

    it('Admin creating a bus - without BusName ', async () => {
      const res = await request(app)
        .post('/api/bus/create')
        .set('x-auth-token', agentRes.body.token)
        .send({
          busType: 'Sleeper',
          fromCity: 'Bangalore',
          toCity: 'Chennai',
          totalSeats: 21,
          pricePerSeat: 700,
          firstStartDate: '2021-11-29',
          travelStartTime: '21:30',
          firstReachingDate: '2021-11-30',
          reachingTime: '06:00',
          frequency: [0, 1, 2, 3, 4, 5, 6],
          seatLayout: [1, 2],
        });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain('busName is required');
    });

    it('Admin creating a bus - without BusType', async () => {
      const res = await request(app)
        .post('/api/bus/create')
        .set('x-auth-token', agentRes.body.token)
        .send({
          busName: 'Tipu Sultan Travels',
          fromCity: 'Bangalore',
          toCity: 'Chennai',
          totalSeats: 21,
          pricePerSeat: 700,
          firstStartDate: '2021-11-29',
          travelStartTime: '21:30',
          firstReachingDate: '2021-11-30',
          reachingTime: '06:00',
          frequency: [0, 1, 2, 3, 4, 5, 6],
          seatLayout: [1, 2],
        });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain('busType is required');
    });

    it('Admin creating a bus - without FromCity', async () => {
      const res = await request(app)
        .post('/api/bus/create')
        .set('x-auth-token', agentRes.body.token)
        .send({
          busName: 'Tipu Sultan Travels',
          busType: 'Sleeper',
          toCity: 'Chennai',
          totalSeats: 21,
          pricePerSeat: 700,
          firstStartDate: '2021-11-29',
          travelStartTime: '21:30',
          firstReachingDate: '2021-11-30',
          reachingTime: '06:00',
          frequency: [0, 1, 2, 3, 4, 5, 6],
          seatLayout: [1, 2],
        });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain('fromCity is required');
    });

    it('Admin creating a bus - without toCity', async () => {
      const res = await request(app)
        .post('/api/bus/create')
        .set('x-auth-token', agentRes.body.token)
        .send({
          busName: 'Tipu Sultan Travels',
          busType: 'Sleeper',
          fromCity: 'Chennai',
          totalSeats: 21,
          pricePerSeat: 700,
          firstStartDate: '2021-11-29',
          travelStartTime: '21:30',
          firstReachingDate: '2021-11-30',
          reachingTime: '06:00',
          frequency: [0, 1, 2, 3, 4, 5, 6],
          seatLayout: [1, 2],
        });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain('toCity is required');
    });

    it('Admin creating a bus - without totalSeats', async () => {
      const res = await request(app)
        .post('/api/bus/create')
        .set('x-auth-token', agentRes.body.token)
        .send({
          busName: 'Tipu Sultan Travels',
          busType: 'Sleeper',
          fromCity: 'Chennai',
          toCity: 'Bangalore',
          pricePerSeat: 700,
          firstStartDate: '2021-11-29',
          travelStartTime: '21:30',
          firstReachingDate: '2021-11-30',
          reachingTime: '06:00',
          frequency: [0, 1, 2, 3, 4, 5, 6],
          seatLayout: [1, 2],
        });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain('totalSeats is required');
    });

    it('Admin creating a bus - without pricePerSeat', async () => {
      const res = await request(app)
        .post('/api/bus/create')
        .set('x-auth-token', agentRes.body.token)
        .send({
          busName: 'Tipu Sultan Travels',
          busType: 'Sleeper',
          fromCity: 'Chennai',
          toCity: 'Bangalore',
          totalSeat: 21,
          firstStartDate: '2021-11-29',
          travelStartTime: '21:30',
          firstReachingDate: '2021-11-30',
          reachingTime: '06:00',
          frequency: [0, 1, 2, 3, 4, 5, 6],
          seatLayout: [1, 2],
        });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain('pricePerSeat is required');
    });

    it('Admin creating a bus - without firstStartDate', async () => {
      const res = await request(app)
        .post('/api/bus/create')
        .set('x-auth-token', agentRes.body.token)
        .send({
          busName: 'Tipu Sultan Travels',
          busType: 'Sleeper',
          fromCity: 'Chennai',
          toCity: 'Bangalore',
          totalSeats: 21,
          travelStartTime: '21:30',
          pricePerSeat: 700,
          firstReachingDate: '2021-11-30',
          reachingTime: '06:00',
          frequency: [0, 1, 2, 3, 4, 5, 6],
          seatLayout: [1, 2],
        });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain('firstStartDate is required');
    });

    it('Admin creating a bus - without firstReachingDate', async () => {
      const res = await request(app)
        .post('/api/bus/create')
        .set('x-auth-token', agentRes.body.token)
        .send({
          busName: 'Tipu Sultan Travels',
          busType: 'Sleeper',
          fromCity: 'Chennai',
          toCity: 'Bangalore',
          totalSeats: 21,
          pricePerSeat: 700,
          travelStartTime: '21:30',
          firstStartDate: '2021-11-30',
          reachingTime: '06:00',
          frequency: [0, 1, 2, 3, 4, 5, 6],
          seatLayout: [1, 2],
        });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain('firstReachingDate is required');
    });

    it('Admin creating a bus - without valid firstStartDate', async () => {
      const res = await request(app)
        .post('/api/bus/create')
        .set('x-auth-token', agentRes.body.token)
        .send({
          busName: 'Tipu Sultan Travels',
          busType: 'Sleeper',
          fromCity: 'Chennai',
          toCity: 'Bangalore',
          totalSeats: 21,
          pricePerSeat: 700,
          travelStartTime: '21:30',
          firstStartDate: '2021-11-20',
          firstReachingDate: '2031-11-30',
          reachingTime: '06:00',
          frequency: [0, 1, 2, 3, 4, 5, 6],
          seatLayout: [1, 2],
        });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain(
        'firstStartDate must be greater than or equal to now'
      );
    });

    it('Admin creating a bus - without valid firstReachingDate', async () => {
      const res = await request(app)
        .post('/api/bus/create')
        .set('x-auth-token', agentRes.body.token)
        .send({
          busName: 'Tipu Sultan Travels',
          busType: 'Sleeper',
          fromCity: 'Chennai',
          toCity: 'Bangalore',
          totalSeats: 21,
          pricePerSeat: 700,
          travelStartTime: '21:30',
          firstStartDate: '2031-11-20',
          firstReachingDate: '2021-11-30',
          reachingTime: '06:00',
          frequency: [0, 1, 2, 3, 4, 5, 6],
          seatLayout: [1, 2],
        });
      expect(res.statusCode).toEqual(400);
      expect(res.text).toContain(
        'The starting time and date should be before the reaching time and date'
      );
    });

    it('Admin creating a bus - without valid frequency', async () => {
      const res = await request(app)
        .post('/api/bus/create')
        .set('x-auth-token', agentRes.body.token)
        .send({
          busName: 'Tipu Sultan Travels',
          busType: 'Sleeper',
          fromCity: 'Chennai',
          toCity: 'Bangalore',
          totalSeats: 21,
          pricePerSeat: 700,
          travelStartTime: '21:30',
          firstStartDate: '2031-11-20',
          firstReachingDate: '2031-11-30',
          reachingTime: '06:00',
          frequency: [0, 2, 2, 3, 4, 5, 6],
          seatLayout: [1, 2],
        });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain('frequency[2] contains a duplicate value');
    });

    it('Admin creating a bus - without seat Layout or total seats', async () => {
      const res = await request(app)
        .post('/api/bus/create')
        .set('x-auth-token', agentRes.body.token)
        .send({
          busName: 'Tipu Sultan Travels',
          busType: 'Sleeper',
          fromCity: 'Chennai',
          toCity: 'Bangalore',
          totalSeats: 20,
          pricePerSeat: 700,
          travelStartTime: '21:30',
          firstStartDate: '2031-11-20',
          firstReachingDate: '2031-11-30',
          reachingTime: '06:00',
          frequency: [0, 1, 2, 3, 4, 5, 6],
          seatLayout: [1, 2],
        });
      expect(res.statusCode).toEqual(400);
      expect(res.text).toContain(
        'The total seats should be a multiple of the seats in the seat layout'
      );
    });

    it('Agent Successfully creates a bus', async () => {
      const res = await request(app)
        .post('/api/bus/create')
        .set('x-auth-token', agentRes.body.token)
        .send({
          busName: 'Tipu Sultan Travels',
          busType: 'Sleeper',
          fromCity: 'Bangalore',
          toCity: 'Chennai',
          totalSeats: 21,
          pricePerSeat: 700,
          firstStartDate: '2031-11-29',
          travelStartTime: '21:30',
          firstReachingDate: '2031-11-30',
          reachingTime: '06:00',
          frequency: [0, 1, 2, 3, 4, 5, 6],
          seatLayout: [1, 2],
        });
      expect(res.statusCode).toEqual(200);
    });

    it('Bus already exists', async () => {
      const res = await request(app)
        .post('/api/bus/create')
        .set('x-auth-token', agentRes.body.token)
        .send({
          busName: 'Tipu Sultan Travels',
          busType: 'Sleeper',
          fromCity: 'Bangalore',
          toCity: 'Chennai',
          totalSeats: 21,
          pricePerSeat: 700,
          firstStartDate: '2031-11-29',
          travelStartTime: '21:30',
          firstReachingDate: '2031-11-30',
          reachingTime: '06:00',
          frequency: [0, 1, 2, 3, 4, 5, 6],
          seatLayout: [1, 2],
        });
      expect(res.statusCode).toEqual(400);
      expect(res.text).toContain('You have already created this bus');
    });
  });
});
