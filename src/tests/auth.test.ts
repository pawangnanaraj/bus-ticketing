const request = require('supertest');
import express, { Application } from 'express';
import { registerUserRouter } from '../router/api/user.router';

const app: Application = express();

app.use(express.json());
app.use('/api/register', registerUserRouter);

describe('Register User, Agent : /register', () => {
  jest.setTimeout(300000);

  //--------------------------Register User ------------------------------
  describe('Register User /', () => {
    it('Without Username', async () => {
      const res = await request(app).post('/api/register').send({
        email: 'pawangr@gmail.com',
        password: '123456',
      });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain('userName is required');
    });

    it('Without email', async () => {
      const res = await request(app).post('/api/register').send({
        userName: 'Pawan GR',
        password: '123456',
      });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain('email is required');
    });

    it('Without password', async () => {
      const res = await request(app).post('/api/register').send({
        userName: 'Pawan GR',
        email: 'pawangr@gmail.com',
      });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain('password is required');
    });

    it('Successfully register user', async () => {
      const res = await request(app)
        .post('/api/register')
        .set('Content-Type', 'application/json')
        .set('Accept', /json/)
        .send({
          userName: 'Pawan GR',
          email: 'pawangr@gmail.com',
          password: '123456',
        });
      expect(res.statusCode).toEqual(200);
      expect(res.text).toBeTruthy();
    });

    it('With duplicate username', async () => {
      const res = await request(app)
        .post('/api/register')
        .set('Accept', /json/)
        .set('Content-Type', 'application/json')
        .send({
          userName: 'Pawan GR',
          email: 'pawangr@gmail.com',
          password: '123456',
        });

      expect(res.text).toContain('user already exists');
      expect(res.statusCode).toEqual(400);
    });
  });

  //--------------------------Register Agent ------------------------------
  describe('Register Agent /', () => {
    it('Without Username', async () => {
      const res = await request(app).post('/api/register/agent').send({
        email: 'pawangr@gmail.com',
        password: '123456',
      });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain('userName is required');
    });

    it('Without email', async () => {
      const res = await request(app).post('/api/register/agent').send({
        userName: 'Pawan GR',
        password: '123456',
      });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain('email is required');
    });

    it('Without password', async () => {
      const res = await request(app).post('/api/register/agent').send({
        userName: 'Pawan GR',
        email: 'pawangr@gmail.com',
      });
      expect(res.statusCode).toEqual(422);
      expect(res.text).toContain('password is required');
    });

    it('Successfully register user', async () => {
      const res = await request(app)
        .post('/api/register/agent')
        .set('Content-Type', 'application/json')
        .set('Accept', /json/)
        .send({
          userName: 'Pawan GR',
          email: 'pawangr@gmail.com',
          password: '123456',
        });
      expect(res.statusCode).toEqual(200);
      expect(res.text).toBeTruthy();
    });

    it('With duplicate username', async () => {
      const res = await request(app)
        .post('/api/register/agent')
        .set('Accept', /json/)
        .set('Content-Type', 'application/json')
        .send({
          userName: 'Pawan GR',
          email: 'pawangr@gmail.com',
          password: '123456',
        });

      expect(res.text).toContain('user already exists');
      expect(res.statusCode).toEqual(400);
    });
  });
});
