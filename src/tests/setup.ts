import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';

// beforeAll(runs once before all the tests)
export let mongo: any;
beforeAll(async () => {
  mongo = await MongoMemoryServer.create();
  const mongoURI = mongo.getUri();
  await mongoose.connect(mongoURI);
});

//afterAll(runs after completion of all the tests)
afterAll(async () => {
  await mongo.stop();
  await mongoose.connection.close();
});
