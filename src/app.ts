import express, { Application, Request, Response, NextFunction } from 'express';
import * as dotenv from 'dotenv';
import { connectDB } from './helpers/connectMongoDB';
import { registerUserRouter } from './router/api/user.router';
import path from 'path';
import { busRouter } from './router/api/bus.router';

import cors from 'cors';
import { authRouter } from './router/api/auth.router';
import { tripRouter } from './router/api/trip.router';
import { ticketRouter } from './router/api/ticket.router';

dotenv.config();

//initialise our app
const app: Application = express();

connectDB();
app.use(cors());
app.use(express.json());

app.use('/api/register', registerUserRouter);
app.use('/api/auth', authRouter);
app.use('/api/bus', busRouter);
app.use('/api/trip', tripRouter);
app.use('/api/ticket', ticketRouter);

app.use(function (error: any, req: Request, res: Response, next: NextFunction) {
  console.log(error.message);
  res
    .status(error.status || error.statusCode || 500)
    .json({ errors: [error.message || 'server Error'] });
});

//Serve static assests in production
if (process.env.NODE_ENV === 'production') {
  //Set static folder

  app.use(express.static('client/build'));
  console.log('server running');
  app.get('*', (req: Request, res: Response) => {
    res.sendFile(path.resolve('/app/client/build/index.html'));
  });
}

const PORT = process.env.PORT || 9000;
app.listen(PORT, () => console.log(`server running ${PORT} `));

export default app;
