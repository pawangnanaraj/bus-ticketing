import express from 'express';
import asyncWrapper from '../../helpers/asyncWrapper';
import { validationMiddleware } from '../../middleware/validation.middleware';
import { authMiddleware } from '../../middleware/auth.middleware';
import { busValidation } from '../../helpers/customValidation';
import busCtrl from '../../controllers/bus.controller';

export const busRouter = express.Router();

//route --> post api/bus/create
//validate bus inputs
//authenticate admin
busRouter.post(
  '/create',
  validationMiddleware(busValidation),
  authMiddleware,
  asyncWrapper(busCtrl.createBus)
);
