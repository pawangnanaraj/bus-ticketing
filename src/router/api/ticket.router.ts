import express from 'express';
import asyncWrapper from '../../helpers/asyncWrapper';
import { validationMiddleware } from '../../middleware/validation.middleware';
import { authMiddleware } from '../../middleware/auth.middleware';
import ticketCtrl from '../../controllers/ticket.controller';
import { ticketValidation } from '../../helpers/customValidation';
export const ticketRouter = express.Router();

//------------------------ View seats of a bus ---------------------------
ticketRouter.get(
  '/seat/:tripId',
  authMiddleware,
  asyncWrapper(ticketCtrl.viewSeats)
);

//------------------------- Get the status of multiple seats -----------------------
ticketRouter.post(
  '/seat',
  authMiddleware,
  asyncWrapper(ticketCtrl.seatDetails)
);

//------------------- Get the available seats in a trip -----------------------
ticketRouter.get(
  '/available/:tripId',
  authMiddleware,
  asyncWrapper(ticketCtrl.availableSeats)
);

//------------------------- Modify the status of a seat -------------------
ticketRouter.put(
  '/seat/:status',
  authMiddleware,
  asyncWrapper(ticketCtrl.updateSeatStatus)
);

//------------------------- Book a ticket --------------------------------
ticketRouter.post(
  '/book/:tripId',
  validationMiddleware(ticketValidation),
  authMiddleware,
  asyncWrapper(ticketCtrl.bookATicket)
);

//------------------------- View ticket by Id -----------------------------
ticketRouter.get(
  '/me/:ticketId',
  authMiddleware,
  asyncWrapper(ticketCtrl.viewTicketById)
);

//------------------------- View my bookings -----------------------------
ticketRouter.get('/me', authMiddleware, asyncWrapper(ticketCtrl.myBookings));
