import express from 'express';
import asyncWrapper from '../../helpers/asyncWrapper';
import tripCtrl from '../../controllers/trip.controller';
import { validationMiddleware } from '../../middleware/validation.middleware';
import { searchTripValidation } from '../../helpers/customValidation';

export const tripRouter = express.Router();

tripRouter.get('/', asyncWrapper(tripCtrl.viewLocations));

tripRouter.get('/trips', asyncWrapper(tripCtrl.searchTrips));

tripRouter.get('/:tripId', asyncWrapper(tripCtrl.viewTripById));
