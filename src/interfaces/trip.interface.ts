export default interface Trip {
  cancelled: boolean;
  busName: string;
  busId: string;
  _id?: string;
  agentId: object;
  busType: string;
  fromCity: string;
  toCity: string;
  totalSeats: number;
  availableSeats: number;
  pricePerSeat: number;
  travelStartDate: string;
  travelStartTime: string;
  reachingDate: string;
  reachingTime: string;
}
