export default interface Ticket {
  cancelled: boolean;
  userId: string;
  tripId: string;
  busName: string;
  fromCity: string;
  toCity: string;
  travelStartDate: string;
  travelStartTime: string;
  reachingDate: string;
  reachingTime: string;
  passengerDetails: [
    {
      passengerName: string;
      seatNumber: number;
      gender: string;
      seatId: string;
    }
  ];
  totalPrice: number;
  emailId: string;
}
