export default interface User {
  userName: string;
  email: string;
  password: string;
  isAgent: boolean;
  id?: string;
  //date: number;
}
