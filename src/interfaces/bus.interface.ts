export default interface Bus {
  busName: string;
  agentId: object;
  busType: string;
  fromCity: string;
  toCity: string;
  totalSeats: number;
  pricePerSeat: number;
  firstStartDate: string;
  travelStartTime: string;
  firstReachingDate: string;
  reachingTime: string;
  frequency: [any];
  validFromDate: string;
  validTillDate: string;
  seatLayout: [number, number];
  _id?: string;
}
