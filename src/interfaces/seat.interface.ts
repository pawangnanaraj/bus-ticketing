export default interface Seat {
  status: string;
  busId: string;
  tripId: string;
  seatNumber: number;
  userId?: string;
}
