import { Request, Response } from 'express';
import Ticket from '../interfaces/Ticket.interface';
import Trip from '../interfaces/trip.interface';
import ticketService from '../services/ticket.service';
import tripService from '../services/trip.service';

//--------------------- Get all seat of a trip -----------------------------
const viewSeats = async (req: Request, res: Response) => {
  const tripId = req.params.tripId;

  const seats = await ticketService.viewSeats(tripId);
  res.json(seats);
};

//-------------------- Get the seat status of setails details ----------------------------
const seatDetails = async (req: Request, res: Response) => {
  const seatId = req.body.seatId;

  const seatDetails = await ticketService.seatDetails(seatId);

  res.json(seatDetails);
};

//-------------------- Get available seats ----------------------------
const availableSeats = async (req: Request, res: Response) => {
  const tripId = req.params.tripId;

  const availableSeats = await ticketService.availableSeats(tripId);

  res.json(availableSeats);
};

//-------------------- Update seats status ----------------------------
const updateSeatStatus = async (req: Request, res: Response) => {
  const seatId = req.body;
  const status = req.params.status;
  const userId = req.body.user.user.id;

  const seatStatus = await ticketService.NewUpdateSeatStatus(
    seatId,
    userId,
    status
  );

  res.json(seatStatus);
};

const bookATicket = async (req: Request, res: Response) => {
  const { passengerDetails, totalPrice, emailId, ...rest } = req.body;

  const ticketDetails = {} as Ticket;

  ticketDetails.userId = req.body.user.user.id;
  ticketDetails.passengerDetails = passengerDetails;
  ticketDetails.tripId = req.params.tripId;
  ticketDetails.totalPrice = totalPrice;
  ticketDetails.emailId = emailId;
  console.log('ticketDetails :', ticketDetails);
  const tripDetails: Trip = await tripService.viewTripById(req.params.tripId);
  ticketDetails.busName = tripDetails.busName;
  ticketDetails.fromCity = tripDetails.fromCity;
  ticketDetails.toCity = tripDetails.toCity;
  ticketDetails.travelStartDate = tripDetails.travelStartDate;
  ticketDetails.travelStartTime = tripDetails.travelStartTime;
  ticketDetails.reachingDate = tripDetails.reachingDate;
  ticketDetails.reachingTime = tripDetails.reachingTime;
  console.log('ticketDetails :', ticketDetails);

  const bookedTicket = await ticketService.bookATicket(ticketDetails);

  res.json(bookedTicket);
};

const viewTicketById = async (req: Request, res: Response) => {
  const ticketId = req.params.ticketId;

  const ticket = await ticketService.viewTicketById(ticketId);

  res.json(ticket);
};
//----------------------------- My bookings -------------------------
const myBookings = async (req: Request, res: Response) => {
  const userId = req.body.user.user.id;

  const allMyBookings = await ticketService.myBookings(userId);

  res.json(allMyBookings);
};

export default {
  viewSeats,
  seatDetails,
  availableSeats,
  updateSeatStatus,
  viewTicketById,
  bookATicket,
  myBookings,
};
