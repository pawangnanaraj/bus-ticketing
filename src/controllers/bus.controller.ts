import { Request, Response } from 'express';
import { busCustomValidation } from '../helpers/customValidation';
import Bus from '../interfaces/bus.interface';
import busService from '../services/bus.service';

//---------------- Create Bus Controller --------------
const createBus = async (req: Request, res: Response) => {
  const userId = req.body.user.user.id;

  const {
    busName,
    busType,
    fromCity,
    toCity,
    totalSeats,
    availableSeats,
    pricePerSeat,
    firstStartDate,
    travelStartTime,
    firstReachingDate,
    reachingTime,
    frequency,
    validFromDate,
    validTillDate,
    seatLayout,
    ...rest
  } = req.body;

  let busDetails = {} as Bus;

  if (busName) busDetails.busName = busName;
  if (busType) busDetails.busType = busType;
  if (fromCity) busDetails.fromCity = fromCity;
  if (toCity) busDetails.toCity = toCity;
  if (totalSeats) busDetails.totalSeats = totalSeats;
  if (pricePerSeat) busDetails.pricePerSeat = pricePerSeat;
  if (firstStartDate) busDetails.firstStartDate = firstStartDate;
  if (travelStartTime) busDetails.travelStartTime = travelStartTime;
  if (firstReachingDate) busDetails.firstReachingDate = firstReachingDate;
  if (reachingTime) busDetails.reachingTime = reachingTime;
  if (frequency) busDetails.frequency = frequency;
  if (validFromDate) busDetails.validFromDate = validFromDate;
  if (validTillDate) busDetails.validTillDate = validTillDate;
  if (seatLayout) busDetails.seatLayout = seatLayout;

  busDetails = busCustomValidation(busDetails);
  //send bus and the agent details to bus Service
  const newBus = await busService.createBus(busDetails, userId);

  //send response
  res.json(newBus);
};

export default {
  createBus,
};
