import { Request, Response } from 'express';
import tripService from '../services/trip.service';
import { InvalidQueryError } from '../middleware/customErrors';
import { parseISO } from 'date-fns';

const viewLocations = async (req: Request, res: Response) => {
  const fromCity = await tripService.getFromCity();
  const toCity = await tripService.getToCity(fromCity);

  res.json({ fromCity, toCity });
};

const searchTrips = async (req: Request, res: Response) => {
  if (!req.query.fromCity) {
    throw new InvalidQueryError('From City is required');
  }
  if (!req.query.toCity) {
    throw new InvalidQueryError('To City is required');
  }
  if (!req.query.travelDate) {
    throw new InvalidQueryError('Travel Date is required');
  }

  console.log(req.query);
  const fromCity: string = req.query.fromCity as string;
  const toCity: string = req.query.toCity as string;
  const travelDate: string = req.query.travelDate as string;

  if (parseISO(travelDate) < new Date(Date.now() - 864e5)) {
    throw new InvalidQueryError('Travel Date should not be a past date');
  }

  const trips = await tripService.searchTrips(fromCity, toCity, travelDate);

  res.json(trips);
};

const viewTripById = async (req: Request, res: Response) => {
  const tripId = req.params.tripId;

  const trip = await tripService.viewTripById(tripId);
  res.json(trip);
};

export default {
  viewLocations,
  searchTrips,
  viewTripById,
};
