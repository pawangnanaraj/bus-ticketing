import { Request, Response } from 'express';
import userService from '../services/user.service';

let isAgent: boolean;

// ---------------Register user and agent --------------------------

const registerUser = async (req: Request, res: Response) => {
  if (req.originalUrl === '/api/register/agent') {
    isAgent = true;
  } else {
    isAgent = false;
  }

  const { userName, email, password } = req.body;

  let token = await userService.registerUser(
    userName,
    email,
    password,
    isAgent
  );

  res.json({ token: token });
};

export default {
  registerUser,
};
