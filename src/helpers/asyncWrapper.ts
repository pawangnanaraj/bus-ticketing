import { Request, Response, NextFunction } from 'express';

export default function promiseWrapper(fn: any) {
  return (req: Request, res: Response, next: NextFunction) => {
    fn(req, res).catch(next);
  };
}
