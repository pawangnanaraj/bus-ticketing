import mongoose from 'mongoose';
import * as dotenv from 'dotenv';

dotenv.config();

let dbAccess = '';

process.env.MONGO_URI ? (dbAccess = process.env.MONGO_URI) : process.exit(1);

export const connectDB = async () => {
  try {
    await mongoose.connect(dbAccess);
    console.log('Connect to DB');
  } catch (error: any) {
    console.log(error.message);

    process.exit(1);
  }
};
