import Joi from 'joi';
import {
  InvalidDateError,
  InvalidSeatsError,
} from '../middleware/customErrors';
import { getDay, parseISO, nextDay, format } from 'date-fns';
import Bus from '../interfaces/bus.interface';
import addDays from 'date-fns/addDays';

//-----------Register Validation -----------
export const registerValidation = Joi.object().keys({
  userName: Joi.string().lowercase().trim().min(3).required(),
  email: Joi.string().lowercase().trim().email().required(),
  password: Joi.string().alphanum().min(6).max(8).required(),
});

//-----------Login Validation -------------
export const loginValidation = Joi.object().keys({
  email: Joi.string().lowercase().trim().email().required(),
  password: Joi.string().alphanum().min(6).max(8).required(),
});

//---------Create Bus Validation -----------
export const busValidation = Joi.object().keys({
  busName: Joi.string().lowercase().trim().required(),
  busType: Joi.string().lowercase().required(),
  fromCity: Joi.string().lowercase().required(),
  toCity: Joi.string().lowercase().required(),
  seatLayout: Joi.array().items(Joi.number()),
  totalSeats: Joi.number().required(),
  pricePerSeat: Joi.number().required(),
  firstStartDate: Joi.date().min('now').required(),
  firstReachingDate: Joi.date().required(),
  travelStartTime: Joi.string().trim().required(),
  reachingTime: Joi.string().trim().required(),
  validFromDate: Joi.date().min('now'),
  validTillDate: Joi.date(),
  frequency: Joi.array().items(Joi.number().min(0).max(6)).unique(),
});

export const busCustomValidation = (busDetails: Bus) => {
  let {
    busName,
    busType,
    fromCity,
    toCity,
    totalSeats,
    pricePerSeat,
    firstStartDate,
    travelStartTime,
    firstReachingDate,
    reachingTime,
    frequency,
    validFromDate,
    validTillDate,
    seatLayout,
  } = busDetails;

  let firstDate: Date,
    validDateFrom: Date,
    validDateTill: Date,
    reachingDate: Date;

  //-------------------Validating start time and Reaching Time --------------

  let startingTime = Date.parse(firstStartDate + 'T' + travelStartTime);
  // console.log('starting time: ', format(startingTime, 'yyyy-MM-dd:hh:mm'));

  let ReachingTime = Date.parse(firstReachingDate + 'T' + reachingTime);
  // console.log('Reaching time: ', format(ReachingTime, 'yyyy-MM-dd:hh:mm'));

  if (ReachingTime < startingTime) {
    throw new InvalidDateError(
      'The starting time and date should be before the reaching time and date'
    );
  }
  let travelTime = ReachingTime - startingTime;

  //-------------------------------------------------------------------------

  firstDate = parseISO(firstStartDate);
  reachingDate = parseISO(firstReachingDate);

  if (frequency.indexOf(getDay(firstDate)) < 0) {
    throw new InvalidDateError(`Invalid From Date and Bus Frequency`);
  }
  if (validFromDate) {
    validDateFrom = parseISO(validFromDate);
    if (firstDate < validDateFrom) {
      throw new InvalidDateError(
        'Start Date should not be before the valid from date'
      );
    }
  } else {
    validDateFrom = firstDate;
  }

  //if valid till date is not given, set default 10 days validity
  if (validTillDate) {
    validDateTill = parseISO(validTillDate);
    if (validDateTill < validDateFrom) {
      throw new InvalidDateError(
        'Valid till Date should not be before the valid from date'
      );
    }
    if (frequency.indexOf(getDay(validDateTill)) < 0) {
      throw new InvalidDateError(`Invalid Till Date and Bus Frequency`);
    }
  } else {
    validDateTill = addDays(validDateFrom, 10);
  }

  //------------------------ seat validity -----------------------------
  let seats = 0;
  for (let seatsIndex = 0; seatsIndex < seatLayout.length; seatsIndex++) {
    seats += seatLayout[seatsIndex];
  }

  if (totalSeats % seats != 0) {
    throw new InvalidSeatsError(
      'The total seats should be a multiple of the seats in the seat layout'
    );
  }

  validFromDate = format(validDateFrom, 'yyyy-MM-dd');
  validTillDate = format(validDateTill, 'yyyy-MM-dd');
  firstStartDate = format(firstDate, 'yyyy-MM-dd');
  firstReachingDate = format(reachingDate, 'yyyy-MM-dd');

  busDetails = {
    ...busDetails,
    validFromDate,
    validTillDate,
    firstStartDate,
    firstReachingDate,
  };

  return busDetails;
};

export const searchTripValidation = Joi.object().keys({
  fromCity: Joi.string().required(),
  toCity: Joi.string().required(),
  travelDate: Joi.date().required(),
});

export const ticketValidation = Joi.object().keys({
  passengerDetails: Joi.array().items({
    passengerName: Joi.string().trim().required(),
    gender: Joi.string().required(),
    seatNumber: Joi.number().required(),
    seatId: Joi.string().required(),
  }),
  totalPrice: Joi.number().required(),
  emailId: Joi.string().lowercase().trim().email().required(),
});
